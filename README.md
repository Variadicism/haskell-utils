# Haskell Utilities

This repository contains generalized utilities for the Haskell programming language.

**NOTE:** All of these modules are meant to be contained inside a folder named "`Utils`"; otherwise, they will not compile.

## Utils.Errors

This is a simple, but essential utility module that includes functions and data to support a "Fallible" pattern of error handling.

"Fallible" in Haskell is simply equivalent to "Either String". Fallible values are meant to be either an error message or a value of the expected type.

Unlike the Haskell Maybe structure, instead of a "Nothing" value that gives purely contextual, often very little or no information as to the nature of the issue, Fallible provides an error message detailing the issue which can be easily added to and propogated up function calls for additional contextual information.

Unlike the Exception structure of Haskell and more common in languages like Java, flow control remains straightforward, explicit, and efficient.

It's worth noting that this `Either String` structure also corresponds nicely with error handling in many parts of the Haskell base libraries, including [Reader](http://hackage.haskell.org/package/text-1.2.3.0/docs/Data-Text-Lazy-Read.html#t:Reader) and [tryIOError](http://hackage.haskell.org/package/base-4.11.1.0/docs/System-IO-Error.html#v:tryIOError).

## Utils.Testing

This module contains utilities meant to write simple Haskell unit testing Haskell programs. It includes utilities to perform some basic checks and construct useful error messages and shut down the program if the checks are not satisfied.

## Utils.Messaging

This module contains a variety of utilities made for creating and sending messages to a terminal in a console application. This includes utilities for custom POSIX terminal colorization; special formatting and output redirection for specific types of messages like errors, warnings, and debug messages; and utilities to aid in formatting data for convenient output into messages.

To view debugging messages, Haskell programs using this module should be compiled with the flag `-DDEBUG`.

## Utils.Monads

This module contains a few utilities to aid in working with the IO monad, especially as a "`Void`" (equivalent to `IO ()`). Also important is the `tryIO` function that runs an IO operation, but also turns the result that may throw an Exception into a Fallible structure for easy and explicit use of the IO Exception.

## Utils.Readers

This module includes a few functions meant to ease the task of "reading" strings and verifying the result. These utilities use the `Reader` API, which also uses the Fallible pattern used by the `Utils.Errors` module.

## Utils.Lists

This module includes a few convenient utilities relating to lists.

## Utils.Tuples

This module includes a few convenient utilities relating to tuples.
