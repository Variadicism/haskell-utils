module Utils.Text (
	(+++),
	(++-),
	(-++),
	(-++-),
	count,
	maybeHead,
	maybeTail,
	findIndex,
	wrap,
	findLastIndex,
	spaceSuffixIf,
	ifNotNull,
	dropPrefix,
	dropSuffix
) where

import qualified Utils.Lists as List (findLastIndex)

import qualified Data.List as List (findIndex)
import Data.Text.Lazy (Text)
import qualified Data.Text.Lazy as Text
import GHC.Int (Int64)

(+++) :: Text -> Text -> Text
(+++) = Text.append

(++-) :: Text -> String -> Text
text ++- string = Text.append text $ Text.pack string

(-++) :: String -> Text -> Text
string -++ text = Text.append (Text.pack string) text

(-++-) :: String -> String -> Text
string1 -++- string2 = Text.concat $ map Text.pack [string1, string2]

count :: (Char -> Bool) -> (Text -> Int)
count filtration = fromIntegral . Text.length . Text.filter filtration

maybeHead :: Text -> Maybe Char
maybeHead text = if Text.null text then Nothing else Just $ Text.head text

maybeTail :: Text -> Maybe Text
maybeTail text = if Text.null text then Nothing else Just $ Text.tail text

findIndex :: (Char -> Bool) -> Text -> Maybe Int64
findIndex matcher = fmap fromIntegral . List.findIndex matcher . Text.unpack

wrap :: Text -> Text -> Text
wrap wrapper center = Text.concat [wrapper, center, wrapper]

findLastIndex :: (Char -> Bool) -> Text -> Maybe Int64
findLastIndex matcher = fmap fromIntegral . List.findLastIndex matcher . Text.unpack

spaceSuffixIf :: Text -> Text
spaceSuffixIf text = if Text.null text then "" else ' ' `Text.cons` text

ifNotNull :: (Text -> Text) -> Text -> Text
ifNotNull f text = if Text.null text then text else f text

dropPrefix :: Text -> Text -> Text
dropPrefix prefix text =
	if prefix `Text.isPrefixOf` text
		then Text.drop (Text.length prefix) text
		else text

dropSuffix :: Text -> Text -> Text
dropSuffix suffix text =
	if suffix `Text.isSuffixOf` text
		then Text.drop (Text.length suffix) text
		else text
