module Utils.Choose (
	requestInput,
	requestChoice,
	requestChoiceAs,
	withChoice,
	withChoiceAs
) where

import Utils.Fallible.Trans
import Utils.Lists (wrap)
import Utils.Messaging (debugM, msg)
import Utils.Processes (Failure(..), Result(..), run, silently)

import Data.List (find, intercalate, isPrefixOf)
import Data.List.Extra (dropPrefix, dropSuffix, replace)
import System.Process (proc, shell)

requestInput :: String -> IOFallible (Maybe String)
requestInput prompt = do
	result <- run ("request input " ++ msg prompt) $
		silently $ proc "i3-input" ["-P", prompt ++ ": "]
	case result.error of
		Nothing -> return $ fmap (dropPrefix "output = ") $
			find ("output =" `isPrefixOf`) $ lines result.stdout
		Just Failure{code=1} -> do
			debugM "No input was given!"
			return Nothing
		Just Failure{message} -> errorT message

requestChoiceAs :: (a -> String) -> [a] -> IOFallible (Maybe a)
requestChoiceAs _ [] = return Nothing
requestChoiceAs _ [soleChoice] = return $ Just soleChoice
requestChoiceAs toReadableID options = do
	result <- run "request choice" $ silently $
		shell $ "dmenu <<< " ++
		(wrap "'" $ intercalate "\n" $ map (replace "'" "'\\''" . toReadableID) options)
	case result.error of
		Nothing -> do
			let choiceID = dropSuffix "\n" result.stdout
			debugM $ msg choiceID ++ " was chosen!"
			return $ find ((choiceID==) . toReadableID) options
		Just Failure{code=1} -> do
			debugM "No choice was made!"
			return Nothing
		Just Failure{message} -> errorT message

requestChoice :: ([String] -> IOFallible (Maybe String))
requestChoice = requestChoiceAs id

withChoiceAs :: (a -> String) -> [a] -> (a -> VoidFallible) -> VoidFallible
withChoiceAs toReadableID choices toDo = do
	maybeChoice <- requestChoiceAs toReadableID choices
	maybe success toDo maybeChoice

withChoice :: ([String] -> (String -> VoidFallible) -> VoidFallible)
withChoice = withChoiceAs id
