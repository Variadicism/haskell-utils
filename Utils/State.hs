module Utils.State (
	StateVar,
	declareState,
	stateIsPresent,
	initState,
	statefully,
	statefullyOutputting,
	getState
) where

import Utils.Fallible.Trans

import Control.Concurrent.MVar (MVar, isEmptyMVar, newEmptyMVar, modifyMVar, putMVar, readMVar)
import GHC.IO (unsafeDupablePerformIO)

type StateVar s = MVar s

declareState :: StateVar s
declareState = unsafeDupablePerformIO newEmptyMVar

stateIsPresent :: (StateVar s -> IO Bool)
stateIsPresent = fmap not . isEmptyMVar

initState :: (StateVar s -> s -> Void)
initState = putMVar

statefullyOutputting :: StateVar s -> (s -> (s, a)) -> IO a
statefullyOutputting stateMVar modify = modifyMVar stateMVar (return . modify)

statefully :: StateVar s -> (s -> s) -> Void
statefully stateMVar modify = statefullyOutputting stateMVar $ \state -> (modify state, ())

getState :: (StateVar s -> IO s)
getState = readMVar
