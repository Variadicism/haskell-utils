module Utils.Maps (
	(+:+),
	(+:),
	mapValueIfPresent,
	splitToMap,
	intercalateMap
) where

import Data.List (intercalate)
import Data.List.Extra (breakOn, splitOn)
import Data.Map (Map, (!?))
import qualified Data.Map as Map
import Data.Tuple.Extra (second)

infixr 5 +:+
(+:+) :: Ord k => (Map k v -> Map k v -> Map k v)
(+:+) = Map.union

infixr 6 +:
(+:) :: Ord k => (k -> v -> Map k v)
(+:) = Map.singleton

mapValueIfPresent :: Ord k => k -> (v -> v) -> Map k v -> Map k v
mapValueIfPresent key mapper map = do
	let modifyValue = \value -> Map.insert key (mapper value) map
	maybe map modifyValue $ map !? key

splitToMap :: String -> String -> String -> Map String String
splitToMap keyValueDelimiter entryDelimiter =
	Map.fromList .
	map (second $ drop $ length keyValueDelimiter) .
	map (breakOn keyValueDelimiter) .
	splitOn entryDelimiter

intercalateMap :: String -> String -> Map String String -> String
intercalateMap keyValueDelimiter entryDelimiter =
	intercalate entryDelimiter .
	map (\(key, value) -> key ++ keyValueDelimiter ++ value) .
	Map.toList