module Utils.Regex (
	Regex,
	RegexOptions(..),
	defaultOptions,
	defaultCaseInsensitiveOptions,
	defaultByLineOptions,
	defaultCaseInsensitiveByLineOptions,
	modOptions,
	CompilationOption(..),
	ExecutionOption(..),
	RegexMatch,
	RegexSubstitute(..),
	re,
	rei,
	re_,
	regexQuasiQuoterWith,
	res,
	resi,
	res_,
	regexSubstitutionQuasiQuoterWith,
	compileRegex,
	compileRegexWith,
	(=~),
	(==~),
	(=^$~),
	(==^$~),
	(*=~),
	compileSubstitution,
	compileSubstitutionWith,
	(//~),
	(*//~),
	escape
--	splitRegex
) where

import Prelude hiding (error)

import Utils.Fallible

import Data.List (intercalate)
import Data.Maybe (isJust)
import qualified GHC.Arr as Array
import qualified GHC.Err as GHC (error)
--import qualified Text.Regex as RegexBase (splitRegex)
import Language.Haskell.TH (Exp, Q)
import qualified Language.Haskell.TH.Syntax as THSyntax
import Language.Haskell.TH.Quote (QuasiQuoter(..))
import qualified Text.Regex.Base.RegexLike as Base
import qualified Text.RE.PCRE as PCRE
import qualified Text.RE.REOptions as REOptions
import qualified "regex-pcre-builtin" Text.Regex.PCRE.Wrap as BasedPCRE

data Regex = Regex PCRE.RE RegexOptions

-- PCRE manual for meanings: https://linux.die.net/man/3/pcreapi
data CompilationOption =
	CompileAnchored |
	CompileAutoCallout |
	CompileCaseInsensitive |
	CompileDollarEndOnly |
	CompileDotAll |
	CompileExtended |
	CompileExtra |
	CompileFirstLine |
	CompileMultiline |
	CompileNoAutoCapture |
	CompileUngreedy |
	CompileUTF8 |
	CompileNoUTF8Check
	deriving (Eq, Show, Bounded, Enum, THSyntax.Lift)

-- PCRE manual for meanings: https://linux.die.net/man/3/pcreapi
data ExecutionOption =
	ExecuteAnchored |
	ExecuteNotStartingLine |
	ExecuteNotEndingLine |
	ExecuteNotEmpty |
	ExecuteNoUTF8Check |
	ExecutePartial
	deriving (Eq, Show, Bounded, Enum, THSyntax.Lift)

data RegexOptions = RegexOptions [CompilationOption] [ExecutionOption]
	deriving (Eq, THSyntax.Lift)

type RegexMatch = (String, Int, [(String, Int)])

data RegexSubstitute = RegexSubstitute Regex String deriving (Eq)

-- private utils

toPCRECompilationOption :: (CompilationOption -> BasedPCRE.CompOption)
toPCRECompilationOption = \case
	CompileAnchored -> BasedPCRE.compAnchored
	CompileAutoCallout -> BasedPCRE.compAutoCallout
	CompileCaseInsensitive -> BasedPCRE.compCaseless
	CompileDollarEndOnly -> BasedPCRE.compDollarEndOnly
	CompileDotAll -> BasedPCRE.compDotAll
	CompileExtended -> BasedPCRE.compExtended
	CompileExtra -> BasedPCRE.compExtra
	CompileFirstLine -> BasedPCRE.compFirstLine
	CompileMultiline -> BasedPCRE.compMultiline
	CompileNoAutoCapture -> BasedPCRE.compNoAutoCapture
	CompileUngreedy -> BasedPCRE.compUngreedy
	CompileUTF8 -> BasedPCRE.compUTF8
	CompileNoUTF8Check -> BasedPCRE.compNoUTF8Check

toPCREExecutionOption :: (ExecutionOption -> BasedPCRE.ExecOption)
toPCREExecutionOption = \case
	ExecuteAnchored -> BasedPCRE.execAnchored
	ExecuteNotStartingLine -> BasedPCRE.execNotBOL
	ExecuteNotEndingLine -> BasedPCRE.execNotEOL
	ExecuteNotEmpty -> BasedPCRE.execNotEmpty
	ExecuteNoUTF8Check -> BasedPCRE.execNoUTF8Check
	ExecutePartial -> BasedPCRE.execPartial

toPCREOptions :: RegexOptions -> PCRE.REOptions
toPCREOptions regexOptions = PCRE.defaultREOptions{
		REOptions.optionsComp = foldl (+) BasedPCRE.compBlank $ map toPCRECompilationOption comp,
		REOptions.optionsExec = foldl (+) BasedPCRE.execBlank $ map toPCREExecutionOption exec }
	where
		(RegexOptions comp exec) = regexOptions

toPCRESubstitution :: RegexSubstitute -> PCRE.SearchReplace PCRE.RE String
toPCRESubstitution (RegexSubstitute (Regex re _) replacement) = PCRE.SearchReplace re replacement

fromMatchArray :: String -> Base.MatchArray -> RegexMatch
fromMatchArray searchString matchGroups =
	(matchedString, matchOffset, map locToMatch submatchLocations)
	where
		(matchLocation:submatchLocations) = Array.elems matchGroups
		locToMatch (offset, length) = (take length $ drop offset searchString, offset)
		(matchedString, matchOffset) = locToMatch matchLocation

-- instances

instance Show RegexOptions where
	show (RegexOptions comp exec) =
		intercalate " & " $ map (intercalate " + ") $ filter (not . null) [map show comp, map show exec]

instance PCRE.IsOption RegexOptions where
	makeREOptions = toPCREOptions

instance Show Regex where
	show (Regex re options) = show options ++ " /\"" ++ PCRE.reSource re ++ "\"/"

instance Eq Regex where
	r1 == r2 = show r1 == show r2

instance Show RegexSubstitute where
	show (RegexSubstitute (Regex re options) replacement) =
		show options ++ " //\"" ++ PCRE.reSource re ++ "///" ++ replacement ++ "\"//"

defaultCompOptionsForMultilineOrNot :: [CompilationOption]
defaultCompOptionsForMultilineOrNot = [CompileExtended, CompileExtra, CompileUTF8]

defaultNonMultilineCompOptions :: [CompilationOption]
defaultNonMultilineCompOptions = [CompileDotAll, CompileDollarEndOnly]

expQuasiQuoter :: String -> (String -> Q Exp) -> QuasiQuoter
expQuasiQuoter name quoteExp = QuasiQuoter {
	quoteExp = quoteExp,
	quotePat = unsupported "patterns",
	quoteType = unsupported "types",
	quoteDec = unsupported "declarations" }
	where
		unsupported thing = GHC.error $
			"Quasi-quoting " ++ thing ++ " is not supported by this " ++ name ++ " QuasiQuoter!"

-- exported utils

defaultOptions :: RegexOptions
defaultOptions = RegexOptions (defaultCompOptionsForMultilineOrNot ++ defaultNonMultilineCompOptions) []

defaultCaseInsensitiveOptions :: RegexOptions
defaultCaseInsensitiveOptions = modOptions defaultOptions (CompileCaseInsensitive:) id

defaultByLineOptions :: RegexOptions
defaultByLineOptions = RegexOptions (CompileMultiline:defaultCompOptionsForMultilineOrNot) []

defaultCaseInsensitiveByLineOptions :: RegexOptions
defaultCaseInsensitiveByLineOptions = modOptions defaultByLineOptions (CompileCaseInsensitive:) id

modOptions :: RegexOptions -> ([CompilationOption] -> [CompilationOption]) -> ([ExecutionOption] -> [ExecutionOption]) -> RegexOptions
modOptions (RegexOptions comp exec) compMod execMod = RegexOptions (compMod comp) (execMod exec)

regexQuasiQuoterWith :: String -> RegexOptions -> QuasiQuoter
regexQuasiQuoterWith name options = expQuasiQuoter name
	\regexString -> [|Regex ($(quoteExp PCRE.re_ regexString) options) options|]

{- Case sensitive quasi-quoter.
   Note that it considers the whole search string (B, Block).
   If you want anchors, "^$", to match line breaks for some reason, specify in `re_`. -}
re :: QuasiQuoter
re = regexQuasiQuoterWith "re" defaultOptions

{- Case INsensitive quasi-quoter.
   Note that it considers the whole search string (B, Block).
   If you want anchors, "^$", to match line breaks for some reason, specify in `re_`. -}
rei :: QuasiQuoter
rei = regexQuasiQuoterWith "rei" defaultCaseInsensitiveOptions

re_ :: QuasiQuoter
re_ = expQuasiQuoter "re_"
	\regexString -> [|\options -> Regex ($(quoteExp PCRE.re_ regexString) options) options|]

regexSubstitutionQuasiQuoterWith :: String -> RegexOptions -> QuasiQuoter
regexSubstitutionQuasiQuoterWith name options = expQuasiQuoter name $ \substitutionString -> [|
	(\(PCRE.SearchReplace re replacement) -> RegexSubstitute (Regex re options) replacement) $
	$(quoteExp PCRE.ed_ substitutionString) options|]

res :: QuasiQuoter
res = regexSubstitutionQuasiQuoterWith "rep" defaultOptions

resi :: QuasiQuoter
resi = regexSubstitutionQuasiQuoterWith "repi" defaultCaseInsensitiveOptions

res_ :: QuasiQuoter
res_ = expQuasiQuoter "rep_" $ \substitutionString -> [|\options ->
	(\(PCRE.SearchReplace re replacement) -> RegexSubstitute (Regex re options) replacement) $
	$(quoteExp PCRE.ed_ substitutionString) options|]

compileRegexWith :: RegexOptions -> String -> Fallible Regex
compileRegexWith options regexString = do
	re <- PCRE.compileRegexWithOptions (toPCREOptions options) regexString
	return $ Regex re options

compileRegex :: (String -> Fallible Regex)
compileRegex = compileRegexWith defaultOptions

infix 4 =~
(=~) :: String -> Regex -> Maybe RegexMatch
searchString =~ (Regex re _) = fmap (fromMatchArray searchString) $
	Base.matchOnce (PCRE.reRegex re) searchString

infix 4 ==~
(==~) :: String -> (Regex -> Bool)
(==~) searchString = isJust . (searchString =~)

infix 4 =^$~
(=^$~) :: String -> Regex -> Maybe [(String, Int)]
(=^$~) target regex = do
	(matchedString, _, capturedGroups) <- target =~ regex
	if length matchedString == length target
		then Just capturedGroups
		else Nothing

infix 4 ==^$~
(==^$~) :: String -> (Regex -> Bool)
(==^$~) searchString = isJust . (searchString =^$~)

infix 4 *=~
(*=~) :: (String -> Regex -> [RegexMatch])
searchString *=~ (Regex re _) = map (fromMatchArray searchString) $
	Base.matchAll (PCRE.reRegex re) searchString

compileSubstitutionWith :: RegexOptions -> String -> String -> Fallible RegexSubstitute
compileSubstitutionWith options regexString substitutionString = do
	(PCRE.SearchReplace re replacement) <-
		PCRE.compileSearchReplaceWithOptions (toPCREOptions options) regexString substitutionString
	return $ RegexSubstitute (Regex re options) replacement

compileSubstitution :: String -> (String -> Fallible RegexSubstitute)
compileSubstitution = compileSubstitutionWith defaultOptions

infixl 6 //~
(//~) :: String -> (RegexSubstitute -> String)
(//~) searchString = (searchString PCRE.?=~/) . toPCRESubstitution

infixl 6 *//~
(*//~) :: String -> RegexSubstitute -> String
(*//~) searchString = (searchString PCRE.*=~/) . toPCRESubstitution

escape :: String -> String
escape = PCRE.escapeREString

-- TODO
--splitRegex :: Regex -> (String -> [String])
--splitRegex re = RegexBase.splitRegex (reRegex re)
