module Utils.Fallible (
	IsError(..),
	FallibleI(..),
	Fallible,
	fallible,
	falueOrDo,
	falueOr,
	assert,
	assume,
	mapError,
	mapErrorM,
	prependToError,
	fromMaybe,
	fromEither,
	toMaybe,
	hasError,
	ifElseError,
	consFal,
	consFalAny,
	sequenceAny,
	tryFilter
) where

import Data.Bifunctor (Bifunctor, bimap, first)
import Data.Maybe (catMaybes)
import GHC.Records (HasField, getField)

-- types

data FallibleI e v = Error e | Falue v

type Fallible v = FallibleI String v

class IsError e where
	prepend :: String -> e -> e
	-- Though semantically similar, this behavior may differ from (<>).
	-- For example, String implements (<>) as (++), but we want String errors to intercalate "\n".
	(<!>) :: e -> e -> e
	infixl 4 <!>

-- instances

instance IsError String where
	prepend = (++)
	(<!>) first = ((first ++ "\n")++)

deriving instance (Eq e, Eq v) => Eq (FallibleI e v)

instance (Show e, Show v) => Show (FallibleI e v) where
	show = fallible (("ERROR: "++) . show) show

instance Functor (FallibleI e) where
	fmap _ (Error e) = Error e
	fmap f (Falue v) = Falue $ f v

instance IsError e => Applicative (FallibleI e) where
	pure = Falue
	(<*>) :: FallibleI e (a -> b) -> FallibleI e a -> FallibleI e b
	(<*>) (Error e1) (Error e2) = Error $ e1 <!> e2
	(<*>) (Error e) _ = Error e
	(<*>) _ (Error e) = Error e
	(<*>) (Falue f) (Falue next) = Falue $ f next

-- instance Alternative (FallibleI e) isn't sensible since there's no good meaning of an `empty` Error.

instance IsError e => Monad (FallibleI e) where
	(>>=) (Error e) _ = Error e
	(>>=) (Falue v) tryNext = tryNext v

instance MonadFail (FallibleI String) where
	fail = Error

instance Bifunctor FallibleI where
	bimap ef vf = \case
		Error e -> Error $ ef e
		Falue v -> Falue $ vf v

-- instance HasField "err" (Maybe v) (e -> FallibleI e v) doesn't work. >:(

instance HasField "maybe" (FallibleI e v) (Maybe v) where
	getField = toMaybe

instance HasField "orDo" (FallibleI e v) ((e -> v) -> v) where
	getField = flip falueOrDo

instance HasField "or" (FallibleI e v) (v -> v) where
	getField = flip falueOr

-- exported

fallible :: (e -> r) -> (v -> r) -> FallibleI e v -> r
fallible ef _ (Error e) = ef e
fallible _ vf (Falue v) = vf v

fromMaybe :: e -> (Maybe v -> FallibleI e v)
fromMaybe error = maybe (Error error) Falue

fromEither :: Either e v -> FallibleI e v
fromEither (Left e) = Error e
fromEither (Right v) = Falue v

toMaybe :: FallibleI e v -> Maybe v
toMaybe (Error _) = Nothing
toMaybe (Falue value) = Just value

falueOrDo :: (e -> v) -> FallibleI e v -> v
falueOrDo f (Error e) = f e
falueOrDo _ (Falue v) = v

falueOr :: v -> (FallibleI e v -> v)
falueOr v = falueOrDo (const v)

mapError :: ((e -> e2) -> FallibleI e v -> FallibleI e2 v)
mapError = first

mapErrorM :: (e -> FallibleI e v) -> (FallibleI e v -> FallibleI e v)
mapErrorM ef = fallible ef Falue

prependToError :: IsError e => String -> (FallibleI e v -> FallibleI e v)
prependToError prefix = mapError $ prepend prefix

assert :: Bool -> (e -> FallibleI e ())
assert condition = ifElseError condition ()

assume :: Show e => (FallibleI e v -> v)
assume = fallible (error . show) id

hasError :: (FallibleI e v -> Bool)
hasError = fallible (const True) (const False)

ifElseError :: Bool -> v -> e -> FallibleI e v
ifElseError condition value error =
	if condition
		then Falue value
		else Error error

consFal :: IsError e => FallibleI e v -> FallibleI e [v] -> FallibleI e [v]
consFal (Error e1) (Error e2) = Error $ e1 <!> e2
consFal (Falue v) (Falue vs) = Falue $ v:vs
consFal (Falue _) (Error e) = Error e
consFal (Error e) (Falue _) = Error e

consFalAny :: IsError e => FallibleI e v -> FallibleI e [v] -> FallibleI e [v]
consFalAny (Error e1) (Error e2) = Error $ e1 <!> e2
consFalAny (Falue v) (Falue vs) = Falue $ v:vs
consFalAny (Falue v) (Error _) = Falue [v]
consFalAny (Error e) (Falue []) = Error e
consFalAny (Error _) (Falue vs) = Falue vs

-- Like `sequence`, but ignores Errors if there are any Falues instead of ignoring Falues if there are Errors.
-- This should be generalized to Foldable or Traversable, but I can't figure out what the start value would be.
sequenceAny :: IsError e => ([FallibleI e a] -> FallibleI e [a])
sequenceAny = foldl (flip consFalAny) (Falue [])

tryFilter :: IsError e => (v -> FallibleI e Bool) -> ([v] -> FallibleI e [v])
-- `catMaybes` and `map` take lists even though they should use any Foldable. :|
-- Maybe I'll rewrite this to take any Foldable some day, but not today.
tryFilter predicate = sequence . catMaybes . map \value ->
	case predicate value of
		Error e -> Just $ Error e
		Falue True -> Just $ Falue value
		Falue False -> Nothing