module Utils.Tuples (
	toList2,
	mapFst2,
	mapSnd2,
	map2,
	apply2,
	intercalate2,
	mapFst3,
	mapSnd3,
	mapThd3,
	uncurry3
) where

toList2 :: (a, a) -> [a]
toList2 (first, second) = [first, second]

mapFst2 :: (a -> b) -> (a, c) -> (b, c)
mapFst2 f (first, second) = (f first, second)

mapSnd2 :: (a -> b) -> (c, a) -> (c, b)
mapSnd2 f (first, second) = (first, f second)

map2 :: (a -> b) -> (a, a) -> (b, b)
map2 f (first, second) = (f first, f second)

apply2 :: ((a -> b), (a -> c)) -> a -> (b, c)
apply2 (f1, f2) p = (f1 p, f2 p)

intercalate2 :: String -> (String, String) -> String
intercalate2 delimiter (a, b) = a ++ delimiter ++ b

mapFst3 :: (a -> r) -> (a, b, c) -> (r, b, c)
mapFst3 f (a, b, c) = (f a, b, c)

mapSnd3 :: (b -> r) -> (a, b, c) -> (a, r, c)
mapSnd3 f (a, b, c) = (a, f b, c)

mapThd3 :: (c -> r) -> (a, b, c) -> (a, b, r)
mapThd3 f (a, b, c) = (a, b, f c)

uncurry3 :: (a -> b -> c -> r) -> ((a, b, c) -> r)
uncurry3 f (a, b, c) = f a b c