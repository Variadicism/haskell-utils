module Utils.Populace (
	Populace(..),
	toList,
	singleton,
	map,
	filter,
	mapIndexed,
	last,
	cons,
	uncons,
	sort,
	sortBy,
	assert,
	assume,
	concat,
	consFal,
	consFalAny,
	snocFalAny,
	sequenceAny,
	updateAt
) where

import Utils.Fallible hiding (assert, assume, consFal, consFalAny, sequenceAny)
import qualified Utils.Fallible as Fallible
import Utils.Messaging (Messagable, msg, msgList)

import Prelude hiding (concat, filter, head, init, last, map, tail)
import qualified Prelude

import qualified Data.List as List
import GHC.Records (HasField, getField)

-- types

data Populace a = Populace { head :: a, tail :: [a] }
	deriving (Eq, Ord)

-- instances

instance Show a => Show (Populace a) where
	show = show . toList

instance Semigroup (Populace a) where
	(<>) p1 p2 = p1{ tail = p1.tail ++ p2.l }

instance Foldable Populace where
	foldr f s p = f p.head $ Prelude.foldr f s p.tail
	length p = length p.tail + 1

instance Functor Populace where
	fmap f p = Populace (f p.head) (Prelude.map f p.tail)

instance Applicative Populace where
	pure x = Populace { head = x, tail = [] }
	-- `assume` is safe: `fs` and `xs` are both Populaces.
	(<*>) :: Populace (a -> b) -> Populace a -> Populace b
	(<*>) fs xs = assume $ fs.l <*> xs.l

-- instance Monoid Populace has no reasonable implementation of `mempty`.

instance Monad Populace where
	return = pure
	-- `assume` is safe: each element of the Populace is converted to a Populace.
	(>>=) p f = assume $ p.l >>= (toList . f)

instance Traversable Populace where
	traverse :: Applicative f => (a -> f b) -> Populace a -> f (Populace b)
	traverse f p = fmap Populace (f p.head) <*> traverse f p.tail

instance HasField "init" (Populace a) [a] where
	getField = init

instance HasField "last" (Populace a) a where
	getField = last

instance Ord a => HasField "sorted" (Populace a) (Populace a) where
	getField = sort

instance HasField "l" (Populace a) [a] where
	getField = toList

instance Messagable a => Messagable (Populace a) where
	msg = msgList . toList

-- exported

toList :: Populace a -> [a]
toList p = p.head:p.tail

singleton :: a -> Populace a
singleton solo = Populace solo []

map :: ((a -> b) -> Populace a -> Populace b)
map = fmap

filter :: (a -> Bool) -> (Populace a -> [a])
filter f = Prelude.filter f . toList

mapIndexed :: (Int -> a -> b) -> Populace a -> Populace b
-- `assume` is safe because we start with a Populace and remove no elements.
mapIndexed f p = assume $ Prelude.map (uncurry f) $ zip [0..] p.l where

init :: Populace a -> [a]
init p = case p.tail of
	[] -> []
	_ -> p.head : Prelude.init p.tail

last :: Populace a -> a
last Populace{head,tail=[]} = head
last Populace{tail} = Prelude.last tail  -- This is safe because `tail` is guaranteed to be non-empty.

cons :: a -> Populace a -> Populace a
cons new p = Populace new $ p.head:p.tail

uncons :: Populace a -> (a, [a])
uncons Populace{head,tail} = (head, tail)

sort :: Ord a => Populace a -> Populace a
-- `assume` is safe because sorting does not remove elements.
sort = assume . List.sort . toList

sortBy :: (a -> a -> Ordering) -> (Populace a -> Populace a)
sortBy f = assume . List.sortBy f . toList

assert :: [a] -> e -> FallibleI e (Populace a)
assert [] error = Error error
assert (first:rest) _ = Falue $ Populace first rest

assume :: [a] -> Populace a
assume l = Fallible.assume $ assert l "This list was empty, but we assumed it wasn't!"

concat :: Populace (Populace a) -> Populace a
concat p = Populace (p.head.head) (p.head.tail ++ concatMap toList p.tail)

-- If `cons` and similar were methods of a class I could implement, all of this code would be unnecessary. >:(
consFal :: IsError e => FallibleI e v -> FallibleI e (Populace v) -> FallibleI e (Populace v)
consFal (Error e1) (Error e2) = Error $ e1 <!> e2
consFal (Falue v) (Falue vs) = Falue $ cons v vs
consFal (Falue _) (Error e) = Error e
consFal (Error e) (Falue _) = Error e

consFalAny :: IsError e => FallibleI e v -> FallibleI e (Populace v) -> FallibleI e (Populace v)
consFalAny (Error e1) (Error e2) = Error $ e1 <!> e2
consFalAny (Falue v) (Falue vs) = Falue $ cons v vs
consFalAny (Falue v) (Error _) = Falue $ singleton v
consFalAny (Error _) (Falue vs) = Falue vs

snocFalAny :: IsError e => FallibleI e (Populace v) -> FallibleI e v -> FallibleI e (Populace v)
-- Note: This /= flip consFalAny because the order of the appending of Errors differs!
-- Use this for foldl; use `consFalAny` for foldr.
snocFalAny (Error pe) (Error ve) = Error $ pe <!> ve
snocFalAny p v = consFalAny v p

sequenceAny :: IsError e => Populace (FallibleI e a) -> FallibleI e (Populace a)
sequenceAny p =
	-- Assume is safe because if the result would be empty, it would result in an Error instead.
	fmap assume $
		Fallible.consFalAny p.head $
		foldr Fallible.consFalAny (Falue []) p.tail

updateAt :: Int -> (a -> a) -> (Populace a -> Populace a)
updateAt index f = mapIndexed \i v -> if i == index then f v else v