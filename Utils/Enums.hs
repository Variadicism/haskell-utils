module Utils.Enums (
	enumValues
) where

enumValues :: (Enum e) => [e]
enumValues = enumFrom (toEnum 0)
