module Utils.StateMachine (
	StateMachine(..)
) where

data StateMachine s i o = StateMachine {
	init :: s,
	run :: s -> i -> (s, o) }