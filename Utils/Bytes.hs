module Utils.Bytes (
	FromBytes,
	fromBytes,
	ToBytes,
	toBytes
) where

import Utils.Fallible

import qualified Data.ByteString.Char8 as StrictByteString
import Data.ByteString.Lazy (ByteString)
import qualified Data.ByteString.Lazy as ByteString
import qualified Data.ByteString.Lazy.Char8 as Char8
import Data.Text.Lazy (Text)
import qualified Data.Text.Lazy.Encoding as Text

class ToBytes b where
	toBytes :: b -> ByteString

class FromBytes b where
	fromBytes :: ByteString -> Fallible b


instance ToBytes ByteString where
	toBytes = id

instance ToBytes StrictByteString.ByteString where
	toBytes = ByteString.fromStrict

instance ToBytes String where
	toBytes = Char8.pack

instance ToBytes Text where
	toBytes = Text.encodeUtf8


instance FromBytes ByteString where
	fromBytes = return . id

instance FromBytes StrictByteString.ByteString where
	fromBytes = return . ByteString.toStrict

instance FromBytes String where
	fromBytes = return . Char8.unpack

instance FromBytes Text where
	fromBytes = mapError show . fromEither . Text.decodeUtf8'
