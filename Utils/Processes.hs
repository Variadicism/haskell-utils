module Utils.Processes (
	HATProcess(..),
	Result(..),
	Failure(..),
	assertSuccess,
	spawn,
	await,
	kill,
	exec,
	run,
	isRunning,
	here,
	silently,
	simulateExec,
	onInterruptSignal,
	nohup
) where

import Utils.Fallible
import Utils.Fallible.Trans
import Utils.HATs
import Utils.Messaging
import Utils.Lists

import Control.Concurrent (ThreadId, myThreadId)
import Control.Exception (throwTo)
import Control.Monad (void)
import Data.Char (toLower)
import Data.List (intercalate)
import Data.Maybe (isNothing)
import Data.String.Utils (strip)
import System.Directory (doesDirectoryExist, getCurrentDirectory)
import System.Exit (ExitCode(..))
import System.IO (Handle, hGetContents)
import System.Posix.Signals (Handler(Catch, Ignore), keyboardSignal, installHandler, sigHUP)
import System.Process (CmdSpec(..), CreateProcess(..), Pid, ProcessHandle, StdStream(..), getPid)
import qualified System.Process as Process (createProcess, getProcessExitCode, terminateProcess, waitForProcess)

-- types

data HATProcess = HATProcess{
	name :: String,
	pid :: Pid,
	spec :: CmdSpec,
	handle :: ProcessHandle,
	stdin :: Maybe Handle,
	stdout :: Maybe Handle,
	stderr :: Maybe Handle,
	threadID :: ThreadId }

data Failure = Failure{
	code :: Int,
	message :: String }

data Result = Result{
	stdout :: String,
	stderr :: String,
	error :: Maybe Failure }

instance Eq HATProcess where
	(==) :: HATProcess -> HATProcess -> Bool
	(==) hat1 hat2 = hat1.pid == hat2.pid

instance Messagable HATProcess where
	msg :: (HATProcess -> String)
	msg = msg . spec

-- instances

instance Messagable CmdSpec where
	msg :: (CmdSpec -> String)
	msg = wrap "`" . \case
		ShellCommand command -> command
		RawCommand programPath args -> intercalate " " $ map (wrap "\"") $ programPath:args

-- private utils

drawProcessLine :: HATProcess -> HATLine
drawProcessLine process@HATProcess{..} = HATLine{
	hatLineName = name,
	hatLineIsOpen = tryIO $ isRunning process,
	cutHATLine = flip kill process }

errorForExitCode :: String -> Int -> String
errorForExitCode processName code = "The " ++ processName ++ " failed with an exit code of " ++ show code ++ "!"

-- exported utils

spawn :: String -> CreateProcess -> IOFallible HATProcess
spawn commandName processSpec = do
	debugM $ "Executing command " ++ msg commandName ++ ":" ++ msg processSpec.cmdspec

	case processSpec.cwd of
		Nothing -> success
		Just workingDir -> do
			workingDirExists <- tryIO $ doesDirectoryExist workingDir
			if workingDirExists
				then success
				else errorT $ "The desired working directory " ++ wrap "\"" workingDir ++ " does not exist!"

	(stdin, stdout, stderr, process) <- tryIO $ Process.createProcess processSpec
	maybePID <- tryIO $ getPid process
	let noPIDError = errorT $ "The PID of the process " ++ msg processSpec.cmdspec ++
		" could not be found immediately after its creation!";
	pid <- maybe noPIDError return maybePID
	threadID <- tryIO myThreadId
	let hatProcess = HATProcess{
		name = commandName,
		spec = processSpec.cmdspec,
		handle = process,
		pid, stdin, stdout, stderr, threadID }
	tieHATLine $ drawProcessLine hatProcess
	return hatProcess

assertSuccess :: Result -> Fallible (String, String)
assertSuccess result = case result.error of
	Nothing -> return (result.stdout, result.stderr)
	Just error -> Error error.message

await :: HATProcess -> IOFallible Result
await HATProcess{..} = do
	debugM $ "Waiting for " ++ msg name ++ " process to complete..."
	exitCode <- tryIO $ Process.waitForProcess handle
	debugM $ "Process " ++ msg name ++ " completed; packaging results..."
	let readOutput = maybe (return "") (tryIO . hGetContents)
	out <- readOutput stdout
	err <- readOutput stderr
	let maybeError = case exitCode of
		ExitSuccess -> Nothing
		ExitFailure code -> do
			let errorBase = errorForExitCode
				(msg name ++ " command " ++ msg spec) code
			let makeNoteFor name content = "---- " ++ if null content
				then name ++ " was empty!"
				else "The following was printed to " ++ map toLower name ++
					":\n" ++ strip content
			let stderrNote = makeNoteFor "Stderr" err
			let stdoutNote = makeNoteFor "Stdout" out
			let message = intercalate "\n" [errorBase, stderrNote, stdoutNote]
			Just $ Failure code message
	return $ Result out err maybeError

kill :: String -> HATProcess -> VoidFallible
kill reason HATProcess{..} = do
	maybePid <- tryIO $ getPid handle
	let pidMsg = maybe "" ((++ " ") . show) maybePid
	debugM $ "Killing process " ++ pidMsg ++ "because " ++ msg reason ++ "..."
	tryIO $ Process.terminateProcess handle

exec :: String -> CreateProcess -> IOFallible (String, String)
exec name spec = run name spec >>= falT . assertSuccess

run :: String -> CreateProcess -> IOFallible Result
run name spec = spawn name spec >>= await

isRunning :: (HATProcess -> IO Bool)
isRunning = fmap isNothing . Process.getProcessExitCode . handle

here :: CreateProcess -> IO CreateProcess
here processSpec = do
	currentDir <- getCurrentDirectory
	return processSpec{ cwd = Just currentDir }

silently :: CreateProcess -> CreateProcess
silently spec = spec{ std_out = CreatePipe, std_err = CreatePipe }

simulateExec :: String -> String -> String -> CreateProcess -> IOFallible (String, String)
simulateExec stdout stderr commandName processSpec = do
	let allCommandParts = case processSpec.cmdspec of
		ShellCommand command -> [command]
		RawCommand programPath args -> (tColor [Bold] programPath):args
	let currentDirColored = maybe (tColor [Cyan] "/?") (tColor [Bold, Cyan]) processSpec.cwd
	tryIO $ putStrLn $ tColor [Cyan] (wrap "\"" commandName) ++ "\n" ++ currentDirColored ++ " $ " ++ (intercalate " " $ map (tColor [Black, LightGrayBG]) allCommandParts)
	return (stdout, stderr)

-- This must be run on the main thread!
onInterruptSignal :: VoidFallible -> Void
onInterruptSignal run = do
	threadID <- myThreadId
	let onSignal = do
		orError <- falM run
		exitStatus <- case orError of
			Error error -> do
				err $ "An error occurred while trying to shut down the program: " ++ error
				return $ ExitFailure (-1)
			Falue _ -> return ExitSuccess
		throwTo threadID exitStatus
	void $ installHandler keyboardSignal (Catch onSignal) Nothing

-- This must be run on the main thread!
nohup :: Void
nohup = void $ installHandler sigHUP Ignore Nothing
