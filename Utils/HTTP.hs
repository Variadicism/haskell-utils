module Utils.HTTP (
	URL,
	HTTPSOptions,
	HTTPQueryParams,
	HTTPHeaders,
	(/:),
	https,
	assertResponse,
	fetchJSON,
	postJSON
) where

import Utils.Fallible
import Utils.Fallible.Trans
import Utils.Lists (listIf)
import Utils.Messaging (debugM, msg, msgShow)
import Utils.Tuples (intercalate2)

import Data.Aeson (ToJSON, toJSON)
import qualified Data.Aeson as Aeson
import qualified Data.ByteString.UTF8 as UTF8BS
import Data.List (intercalate)
import Data.Maybe (isJust)
import qualified Data.Text as Text
import Network.HTTP.Req (GET(GET), POST(POST), NoReqBody(NoReqBody), ReqBodyJson(ReqBodyJson), req, runReq)
import qualified Network.HTTP.Req as Req

-- types

type URL = Req.Url 'Req.Https

type HTTPSOptions = Req.Option 'Req.Https

type HTTPQueryParams = [(String, String)]

type HTTPHeaders = [(String, String)]

-- private utils

assembleReqOptions :: HTTPQueryParams -> HTTPHeaders -> HTTPSOptions
assembleReqOptions queryParams headers = do
	let tupleToReqHeader (key, value) =
		Req.header (UTF8BS.fromString key) (UTF8BS.fromString value)
	let headerOptions = foldl (\o -> (o <>) . tupleToReqHeader) mempty headers

	let queryParamOptions =
		foldl (\o (key, value) -> o <> (Text.pack key Req.=: value)) mempty queryParams

	queryParamOptions <> headerOptions

debugRequest :: Monad m =>
	String -> URL -> HTTPQueryParams -> HTTPHeaders -> Maybe Aeson.Value -> m ()
debugRequest requestDescription url queryParams headers maybeBody = do
	let debugMessageStart = "Fetching " ++ msg requestDescription ++
			" from " ++ show url ++ "..."
	let moreInfo = [
		(not $ null queryParams, ("query params",
			intercalate "&" $ map (intercalate2 "=") queryParams)),
		(not $ null headers, ("headers",
			intercalate "\n" $ map (('\t':) . intercalate2 ": ") headers)),
		(isJust maybeBody, ("body", maybe "" (show . toJSON) maybeBody))]
	debugM $ debugMessageStart ++
			concat (map (('\n':) . intercalate2 ":\n") $ listIf moreInfo)

assertAndDebugJSONResponse ::
	String -> String -> URL -> (Req.JsonResponse Aeson.Value) -> Fallible Aeson.Value
assertAndDebugJSONResponse requestDescription methodName url response = do
	assertResponse methodName url response
	
	let responseValue = Req.responseBody response :: Aeson.Value
	debugM $ msg requestDescription ++ " response received:\n" ++ show responseValue
	return responseValue

-- exported utils

(/:) :: URL -> String -> URL
(/:) url = (url Req./:) . Text.pack

https :: (String -> URL)
https = Req.https . Text.pack

assertResponse :: (Req.HttpResponse response) => String -> URL -> response -> Fallible ()
assertResponse method url response = do
	let status = Req.responseStatusCode response
	if status > 299 then
		Error $ "The " ++ show method ++ " call to " ++ msgShow url ++ " received a " ++
			show status ++ " response: " ++
			msg (UTF8BS.toString $ Req.responseStatusMessage response)
	else
		return ()

fetchJSON :: String -> URL -> HTTPQueryParams -> HTTPHeaders -> IOFallible Aeson.Value
fetchJSON valuesDescription url queryParams headers = do
	let requestDesciption = "Fetching " ++ valuesDescription ++ " (JSON)"
	debugRequest requestDesciption url queryParams headers Nothing

	response <- runReq Req.defaultHttpConfig $
		req GET url NoReqBody Req.jsonResponse $
		assembleReqOptions queryParams headers

	falT $ assertAndDebugJSONResponse requestDesciption "GET" url response

postJSON :: ToJSON body =>
	String -> URL -> HTTPQueryParams -> HTTPHeaders -> Maybe body -> IOFallible Aeson.Value
postJSON actionDescription url queryParams headers maybeBody = do
	let requestDescription = actionDescription ++ " (JSON)"
	debugRequest requestDescription	url queryParams headers $ fmap toJSON maybeBody

	let reqOptions = assembleReqOptions queryParams $ (("Content-Type", "application/json"):headers)
	-- This `case` is necessary to match HttpMethod and body types for some reason.
	let request = case maybeBody of
		Nothing -> req POST url NoReqBody Req.jsonResponse reqOptions
		Just body -> req POST url (ReqBodyJson body) Req.jsonResponse reqOptions
	response <- runReq Req.defaultHttpConfig request

	falT $ assertAndDebugJSONResponse requestDescription "POST" url response
		