module Utils.WebSockets (
	MessageTransformer,
	WebSocket(..),
	PingPongOptions(..),
	defaultPort,
	defaultCloseMessage,
	defaultWebSocketListeners,
	defaultPingPongOptions,
	isConnected,
	errIfDisconnected,
	awaitMessage,
	awaitMessageAs,
	whenMessage,
	whenMessageAs,
	controlMessageOnly,
	pingOnly,
	pongOnly,
	closeOnly,
	dataMessageOnly,
	textMessageOnly,
	jsonMessageOnly,
	serve,
	serveAsync,
	connect,
	connectAt,
	connectSecurely,
	connectSecurelyAt,
	reconnect,
	send,
	sendText,
	sendJSON,
	pingPong,
	pingPongWith,
	close,
	closeWith
) where

import Utils.Bytes
import Utils.Fallible
import Utils.Fallible.Trans
import Utils.HATs
import Utils.JSON ()
import Utils.Lists
import Utils.Messaging

import Control.Concurrent (threadDelay)
import Control.Concurrent.MVar (newEmptyMVar, putMVar, takeMVar)
import Control.Monad ((>=>), void, when)
import Data.Aeson (ToJSON)
import qualified Data.Aeson as Aeson
import qualified Data.Aeson.Text as Aeson
import qualified Data.ByteString.Char8 as StrictByteString
import Data.ByteString.Lazy (ByteString)
import qualified Data.ByteString.Lazy as ByteString
import Data.IORef (IORef, atomicModifyIORef, newIORef, readIORef, writeIORef)
import Data.Maybe (isJust)
import Data.Text.Lazy (Text)
import qualified Data.Text.Lazy as Text
import Data.Word (Word16)
import Network.Socket (PortNumber)
import Network.WebSockets (ControlMessage(..), DataMessage, Message(..))
import qualified Network.WebSockets as WebSockets
import qualified Wuss as Wuss (runSecureClient)

-- constants

defaultPort = 80

defaultCloseMessage :: Text
defaultCloseMessage = "Bye bye!"

defaultWebSocketListeners :: String -> IORef (Maybe WebSockets.Connection) -> [WebSocketListener]
defaultWebSocketListeners name connectionRef = [WebSocketListener{
	wslName = "disconnect on close",
	wslTransformer = closeOnly,
	wslOnReceive = \_ -> do
		debugM $ "Web socket " ++ msg name ++ " received close message; closing..."
		tryIO $ closeConnectionRefWithoutMessage connectionRef
		return False
}]

-- types

type MessageTransformer a = Message -> Fallible a

data WebSocketListener = forall wsm . WebSocketListener{
	wslName :: String,
	wslTransformer :: MessageTransformer wsm,
	wslOnReceive :: wsm -> IOFallible Bool -- return True if it should continue listening
}

data WebSocket = WebSocket{
	wsName :: String,
	wsHost :: String,
	wsPort :: Int,
	wsHostPath :: String,
	wsConnectionRef :: IORef (Maybe WebSockets.Connection),
	wsListenersRef :: IORef [WebSocketListener] }

instance Eq WebSocket where
	ws1 == ws2 = and $ map (\f -> f ws1 == f ws2) [wsName, wsHost, show . wsPort, wsHostPath]

data PingPongOptions ping pong = PingPongOptions {
	ppoPingTransformer :: MessageTransformer ping,
	ppoPongTransformer :: MessageTransformer pong,
	ppoSendPing :: WebSocket -> ping -> VoidFallible,
	ppoSendPong :: WebSocket -> pong -> VoidFallible,
	ppoMakePingExpectingPong :: IO (
			ping,  -- the body to give ping messages sent by this program
			pong -> Fallible ()  -- determine if a pong received matches the ping expected
		),
	ppoPongForPing :: ping -> IOFallible pong,  -- the body to give pongs to respond to pings
	ppoSeconds :: Int,  -- the number of seconds to wait between pings
	ppoOnNoPong :: VoidFallible  -- what to do if the ping time expires without receiving its expected pong
}

-- private utils

socketOn :: String -> String -> Int -> String -> Maybe WebSockets.Connection -> IO WebSocket
socketOn name host port path maybeConnection = do
	connectionRef <- newIORef maybeConnection
	listenersRef <- newIORef $ defaultWebSocketListeners name connectionRef
	return WebSocket{
		wsName = name,
		wsHost = host,
		wsPort = port,
		wsHostPath = path,
		wsConnectionRef = connectionRef,
		wsListenersRef = listenersRef }

listenToSocket :: WebSocket -> IOFallible HAT
listenToSocket WebSocket{..} =
	haltableAsync ("receive data on web socket " ++ msg wsName) $ do
		let receiveLoop = do
			maybeConnection <- tryIO $ readIORef wsConnectionRef
			case maybeConnection of
				Nothing -> do
					debugM $ "Connection lost for web socket " ++ msg wsName ++
						"; closing receive loop..."
					success
				Just connection -> do
					message <- tryIO $ WebSockets.receive connection
					debugM $ "Message received via web socket " ++ msg wsName ++
						": " ++ msgShow message
					listeners <- tryIO $ readIORef wsListenersRef

					let runListener listener@WebSocketListener{..} =
						case wslTransformer message of
							Error rejection -> return (True, listener, Just rejection)
							Falue transformedValue -> do
								keepListening <- wslOnReceive transformedValue
								return (keepListening, listener, Nothing)
					keepListeningAndListenersAndMaybeRejections <- mapM runListener listeners

					let foldRejectionsOrAcceptances
							(hasAcceptances, messages)
							(WebSocketListener{..}, maybeRejection) =
						case maybeRejection of
							Nothing -> do
								let acceptanceMessage = "Web socket listener " ++ msg wslName ++
									" accepted message; running listener..."
								(True,) $ if hasAcceptances
									then acceptanceMessage:messages
									else [acceptanceMessage]
							Just rejection ->
								if hasAcceptances then
									(True, messages)
								else do
									let rejectionMessage = "Web socket listener " ++ msg wslName ++
										" rejected message: " ++ msg rejection
									(False, rejectionMessage:messages)
					let (accepted, messages) = foldl foldRejectionsOrAcceptances (False, []) $
						map (\(_, listener, maybeRejection) -> (listener, maybeRejection))
						keepListeningAndListenersAndMaybeRejections
					when (not accepted) $ debugM "No web socket listener recognized the message."
					mapM_ debugM messages

					tryIO $ writeIORef wsListenersRef $
						listIf $ map (\(keepListening, listener, _) -> (keepListening, listener))
						keepListeningAndListenersAndMaybeRejections
					receiveLoop
		receiveLoop

closeConnectionRefWithoutMessage :: (IORef (Maybe WebSockets.Connection) -> Void)
closeConnectionRefWithoutMessage = flip writeIORef Nothing

closeWithoutMessage :: WebSocket -> Void
closeWithoutMessage = closeConnectionRefWithoutMessage . wsConnectionRef

textMessageOnlyWithoutDecoding :: (MessageTransformer ByteString)
textMessageOnlyWithoutDecoding = dataMessageOnly >=> \case
	WebSockets.Text bytestring _ -> return bytestring
	WebSockets.Binary _ -> Error "The message was binary, not text!"

connectUsing :: String ->
	(String -> Int -> String -> (WebSockets.Connection -> Void) -> Void) ->
	String -> Int -> String -> IOFallible WebSocket
connectUsing name connectFunction host port path = do
	let address = host ++ ":" ++ show port ++ path
	debugM $ "Connecting to " ++ address ++ "..."

	socket@WebSocket{..} <- tryIO $ socketOn name host port path Nothing

	tryIO $ connectFunction host port path $ \connection -> do
		debugM $ "Connected to " ++ address ++ "!"
		writeIORef wsConnectionRef $ Just connection
		(fallible err (void . return) =<<) $ falM $ listenToSocket socket

	return socket

-- instances

instance Messagable WebSocket where
	msg WebSocket{..} = "web socket " ++ msg wsName ++ " at " ++ msg
		(wsHost ++ ":" ++ show wsPort ++ wsHostPath)

instance ToBytes DataMessage where
	toBytes (WebSockets.Text bytes _) = bytes
	toBytes (WebSockets.Binary bytes) = bytes

-- exported utils

defaultPingPongOptions :: PingPongOptions ByteString ByteString
defaultPingPongOptions = PingPongOptions{
	ppoPingTransformer = pingOnly,
	ppoPongTransformer = pongOnly,
	ppoSendPing = \socket -> send socket . WebSockets.ControlMessage . Ping,
	ppoSendPong = \socket -> send socket . WebSockets.ControlMessage . Pong,
	ppoMakePingExpectingPong = return (ByteString.empty, const $ return ()),
	ppoPongForPing = \_ -> return ByteString.empty,
	ppoSeconds = 10,
	ppoOnNoPong = success }

isConnected :: WebSocket -> IO Bool
isConnected WebSocket{..} = fmap isJust $ readIORef wsConnectionRef

errIfDisconnected :: WebSocket -> IOFallible WebSockets.Connection
errIfDisconnected WebSocket{..} = do
	maybeConnection <- tryIO $ readIORef wsConnectionRef
	case maybeConnection of
		Nothing -> errorT $ msg wsName ++ " has no connection!"
		Just connection -> return connection

controlMessageOnly :: (MessageTransformer ControlMessage)
controlMessageOnly = \case
	WebSockets.ControlMessage controlMessage -> return controlMessage
	WebSockets.DataMessage _ _ _ _ -> Error "The message was data, not control!"

pingOnly :: (MessageTransformer ByteString)
pingOnly = controlMessageOnly >=> \case
	Ping message -> return message
	_ -> Error "The message was not a ping!"

pongOnly :: (MessageTransformer ByteString)
pongOnly = controlMessageOnly >=> \case
	Pong message -> return message
	_ -> Error "The message was not a pong!"

closeOnly :: (MessageTransformer (Word16, ByteString))
closeOnly = controlMessageOnly >=> \case
	Close code message -> return (code, message)
	_ -> Error "The message was not a close!"

dataMessageOnly :: (MessageTransformer DataMessage)
dataMessageOnly = \case
	ControlMessage _ -> Error "The message was control, not data!"
	DataMessage _ _ _ dataMessage -> return dataMessage

textMessageOnly :: (MessageTransformer Text)
textMessageOnly = textMessageOnlyWithoutDecoding >=> fromBytes

jsonMessageOnly :: (MessageTransformer Aeson.Value)
jsonMessageOnly = textMessageOnlyWithoutDecoding >=> fromBytes

whenMessageAs :: String -> (MessageTransformer a) -> WebSocket -> (a -> IOFallible Bool) -> VoidFallible
whenMessageAs name transformer WebSocket{..} onReceive = do
	let listener = WebSocketListener {
		wslName = name,
		wslTransformer = transformer,
		wslOnReceive = onReceive }
	tryIO $ atomicModifyIORef wsListenersRef $ (,()) . (listener:)

whenMessage :: String -> (WebSocket -> (Message -> IOFallible Bool) -> VoidFallible)
whenMessage name = whenMessageAs name return

awaitMessageAs :: String -> (MessageTransformer a) -> WebSocket -> IOFallible a
awaitMessageAs name transformer socket = do
	messageMVar <- tryIO $ newEmptyMVar
	whenMessageAs name transformer socket $ \message -> do
		tryIO $ putMVar messageMVar message
		return False
	mFal $ takeMVar messageMVar

awaitMessage :: String -> (WebSocket -> IOFallible Message)
awaitMessage name = awaitMessageAs name return

serve :: String ->  -- server name
	String ->  -- host
	Int ->  -- port
	(WebSockets.PendingConnection -> IOFallible String) ->  -- should accept with name
	(WebSocket -> VoidFallible) ->  -- app
	VoidFallible
serve name host port tryAcceptAndName app = do
	debugM $ "Running web socket server " ++ msg name ++ "..."
	tryIO $ WebSockets.runServer host port $ \pendingConnection -> do
		connectionNameOrError <- falM $ tryAcceptAndName pendingConnection
		case connectionNameOrError of
			Error error -> do
				debugM $ "Rejected request to connect to web socket server " ++ msg name ++
					": " ++ msg error
				WebSockets.rejectRequest pendingConnection $ StrictByteString.pack error
			Falue socketName -> do
				debugM $ "Accepted request to connect to web socket server " ++ msg name ++
					" with new socket name " ++ msg socketName ++ "!"
				connection <- WebSockets.acceptRequest pendingConnection
				let decodedPath = fromBytes $ ByteString.fromStrict $ WebSockets.requestPath $
					WebSockets.pendingRequest pendingConnection
				let handleUnreadablePath exception = do
					warn $ "The path could not be decoded from the request head. '/' will be assumed instead. Here is the cause: " ++ msgShow exception
					return "/"
				path <- fallible handleUnreadablePath (return . Text.unpack) decodedPath
				socket <- socketOn socketName host port path $ Just connection
				(fallible err (void . return) =<<) $ falM $ listenToSocket socket
				appResult <- falM $ app socket
				fallible err return appResult

type SocketCloser = Void
serveAsync :: String ->  -- server name
	String ->  -- host
	Int ->  -- port
	(WebSockets.PendingConnection -> IOFallible String) ->  -- should accept with name
	(WebSocket -> SocketCloser -> VoidFallible) ->  -- app
	VoidFallible
serveAsync name host port tryAcceptAndName app =
	serve name host port tryAcceptAndName $ \socket -> do
		closingMVar <- tryIO newEmptyMVar
		let allowClosing = putMVar closingMVar ()
		haltableAsync (wsName socket) $ app socket allowClosing
		whenMessageAs "close served connection" closeOnly socket $ \_ -> do
			tryIO allowClosing
			return False
		tryIO $ takeMVar closingMVar

connectSecurelyAt :: String -> String -> Int -> (String -> IOFallible WebSocket)
connectSecurelyAt name host port = connectUsing name
	(\host port -> Wuss.runSecureClient host (fromIntegral port :: PortNumber))
	host port

connectSecurely :: String -> String -> IOFallible WebSocket
connectSecurely name host = connectSecurelyAt name host defaultPort "/"

connectAt :: String -> (String -> Int -> String -> IOFallible WebSocket)
connectAt name = connectUsing name WebSockets.runClient

connect :: String -> String -> IOFallible WebSocket
connect name host = connectAt name host defaultPort "/"

reconnect :: WebSocket -> VoidFallible
reconnect WebSocket{..} = tryIO $
	WebSockets.runClient wsHost wsPort wsHostPath $ writeIORef wsConnectionRef . Just

send :: (WebSocket -> Message -> VoidFallible)
send socket message = do
	connection <- errIfDisconnected socket
	debugM $ "Sending web message: " ++ msgShow message
	tryIO $ WebSockets.send connection message

sendText :: (WebSocket -> Text -> VoidFallible)
sendText socket text = do
	connection <- errIfDisconnected socket
	tryIO $ WebSockets.sendTextData connection text

sendJSON :: ToJSON j => WebSocket -> (j -> VoidFallible)
sendJSON socket = sendText socket . Aeson.encodeToLazyText

pingPongWith :: (ToBytes ping, ToBytes pong) =>
	PingPongOptions ping pong -> WebSocket -> VoidFallible
pingPongWith PingPongOptions{..} socket@WebSocket{..} = do
	whenMessageAs ("pong pings to " ++ msg wsName) ppoPingTransformer socket $ \pingBody -> do
		debugM $ "Web socket " ++ msg wsName ++ " received a ping! Responding with pong..."
		pongBody <- ppoPongForPing pingBody
		ppoSendPong socket pongBody
		return True

	(pingBody, pongMatchesExpected) <- tryIO ppoMakePingExpectingPong
	receivedPongRef <- tryIO $ newIORef False
	whenMessageAs ("read pongs for " ++ msg wsName) ppoPongTransformer socket $ \pongBody -> do
		debugM $ "Web socket " ++ msg wsName ++ " received a pong! Validating..."
		falT $ pongMatchesExpected pongBody
		debugM $ "The pong message for " ++ msg wsName ++ " matches expectations!"
		tryIO $ writeIORef receivedPongRef True
		return True
	let loopPing = do
		haltIfNeeded

		debugM $ "Pinging web socket " ++ msg wsName ++ "..."
		ppoSendPing socket pingBody

		tryIO $ threadDelay (ppoSeconds * 1000000)

		receivedPong <- tryIO $ readIORef receivedPongRef
		if not receivedPong then do
			debugM $ "No valid pong was received on web socket " ++ msg wsName ++
				" in " ++ show ppoSeconds ++ " seconds! " ++
				"Stopping ping pong and running fallback callback..."
			ppoOnNoPong
		else do
			tryIO $ writeIORef receivedPongRef False
			loopPing

	haltableAsync ("ping loop for web socket " ++ msg wsName) loopPing
	success

pingPong :: (WebSocket -> VoidFallible)
pingPong = pingPongWith defaultPingPongOptions

closeWith :: ToBytes b => (b -> WebSocket -> VoidFallible)
closeWith closeMessage socket@WebSocket{..} = do
	connection <- errIfDisconnected socket
	debugM $ "Sending close message to web socket " ++ msg wsName ++ "..."
	tryIO $ WebSockets.sendClose connection $ toBytes closeMessage
	tryIO $ closeWithoutMessage socket
	debugM $ "Web socket " ++ msg wsName ++ " closed!"

close :: (WebSocket -> VoidFallible)
close = closeWith defaultCloseMessage
