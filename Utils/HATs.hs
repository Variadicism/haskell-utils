module Utils.HATs (
	HAT(..),
	HATLine(..),
	haltableAsync,
	haltTask,
	haltTaskIn,
	haltThis,
	haltIfNeeded,
	haltIfNeededWith,
	tieHATLine,
	tieHATLineTo,
	cutHATLineIfOpen
) where

import Utils.Fallible
import Utils.Fallible.Trans
import Utils.Messaging
import Utils.Lists

import Control.Concurrent (ThreadId, forkIO, killThread, myThreadId, newEmptyMVar, putMVar, takeMVar)
import Data.Maybe (isJust)
import Data.IORef (IORef, atomicModifyIORef, newIORef, readIORef)
import qualified Data.Map as Map
import Data.Map (Map, (!?))
import GHC.IO (unsafeDupablePerformIO)

-- types

data HATLine = HATLine{
	hatLineName :: String,
	hatLineIsOpen :: IOFallible Bool,
	cutHATLine :: String -> VoidFallible }

data HAT = HAT{ -- HaltableAsynchronousTask
	hatName :: String,
	hatHaltReason :: Maybe String,
	hatLines :: [HATLine],
	hatThreadID :: ThreadId }

-- instances

instance Messagable ThreadId where
	msg :: (ThreadId -> String)
	msg = show

-- private utils

newHAT :: String -> ThreadId -> HAT
newHAT name threadID = HAT{
	hatName = name,
	hatThreadID = threadID,
	hatHaltReason = Nothing,
	hatLines = [] }

hatStoreRef :: IORef (Map ThreadId HAT)
hatStoreRef = unsafeDupablePerformIO $ newIORef Map.empty

tryGetHAT :: ThreadId -> IO (Maybe HAT)
tryGetHAT threadID = do
	hatStore <- readIORef hatStoreRef
	return $ hatStore !? threadID

tryGetThisHAT :: IO (Maybe HAT)
tryGetThisHAT = myThreadId >>= tryGetHAT

notInHATError :: String -> String
notInHATError functionName = wrap "`" functionName ++ " (or similar) was called outside of a HAT! Perhaps `forkIO` was used instead of `haltableAsync`."

getHAT :: String -> ThreadId -> IOFallible HAT
getHAT functionName threadID = do
	maybeThisHAT <- tryIO $ tryGetHAT threadID
	maybe (errorT $ notInHATError functionName) return maybeThisHAT

modifyHATIfPresentOrElseMaybeError :: ThreadId -> (HAT -> Fallible HAT) -> Maybe String -> VoidFallible
modifyHATIfPresentOrElseMaybeError threadID modify maybeNotPresentError = do
	maybeError <- tryIO $ atomicModifyIORef hatStoreRef $ \hatStore ->
		case hatStore !? threadID of
			Nothing -> (hatStore, maybeNotPresentError)
			Just hat -> do
				let returnError = \error ->
					(hatStore, Just $ "HAT modification function failed: " ++ error)
				let insertHAT = \newHAT ->
					(Map.insert threadID newHAT hatStore, Nothing)
				fallible returnError insertHAT $ modify hat
	maybe success errorT maybeError

modifyHATIfPresent :: ThreadId -> (HAT -> Fallible HAT) -> VoidFallible
modifyHATIfPresent threadID modify = modifyHATIfPresentOrElseMaybeError threadID modify Nothing

modifyHAT :: ThreadId -> (HAT -> Fallible HAT) -> VoidFallible
modifyHAT threadID modify = modifyHATIfPresentOrElseMaybeError threadID modify $
	Just $ "No HAT to modify was found for the thread ID " ++ show threadID ++ "!"

haltIfNeededWith' :: Bool -> VoidFallible -> VoidFallible
haltIfNeededWith' showPrep beforeKill = do
	maybeThisHAT <- tryIO tryGetThisHAT
	case maybeThisHAT of
		Nothing -> errorT $ notInHATError "haltIfNeeded"
		Just HAT{..} -> case hatHaltReason of
			Nothing -> do
				debugM $ "No halt needed for " ++ msg hatName ++ "..."
				success
			Just reason -> do
				if showPrep then
					debugM $ "Prepping to halt " ++ msg hatName ++
						" because " ++ msg reason ++ "..."
				else success
				beforeKill
				debugM $ "Killing this thread " ++ msg hatThreadID ++ " for task " ++ msg hatName ++ " because " ++ msg reason ++ "..."
				tryIO $ killThread hatThreadID

drawSubHATLine :: HAT -> HATLine
drawSubHATLine hat = HATLine {
	hatLineName = hatName hat ++ " line",
	hatLineIsOpen = fmap (isJust . hatHaltReason) $ getHAT "drawSubHATLine" $ hatThreadID hat,
	cutHATLine = flip haltTask hat }

-- exported utils

haltableAsync :: String -> VoidFallible -> IOFallible HAT
haltableAsync name toRun = do
	debugM $ "Making haltable async task (HAT) " ++ msg name ++ "..."
	waitToStartMVar <- tryIO $ newEmptyMVar
	threadID <- tryIO $ forkIO $ takeMVar waitToStartMVar >> elseErr toRun
	let hat = newHAT name threadID
	tryIO $ atomicModifyIORef hatStoreRef $ (,()) . Map.insert threadID hat
	tieHATLine $ drawSubHATLine hat
	tryIO $ putMVar waitToStartMVar ()
	return hat

haltTaskIn :: String -> ThreadId -> VoidFallible
haltTaskIn reason threadID = do
	debugM $ "Finding HAT to halt for " ++ msg threadID ++ "..."
	HAT{..} <- getHAT "haltTask" threadID

	debugM $ "Found HAT " ++ msg hatName ++ " for thread " ++ msg threadID ++ "; setting halt reason..."
	-- TODO: combine get and modify calls
	modifyHAT threadID $ \hat -> return hat{ hatHaltReason = Just reason, hatLines = [] }

	debugM $ "Cutting " ++ show (length hatLines) ++ " HAT lines..."
	mapM_ (cutHATLineIfOpen reason) hatLines

haltTask :: String -> HAT -> VoidFallible
haltTask reason = haltTaskIn reason . hatThreadID

haltThis :: String -> VoidFallible
haltThis reason = tryIO myThreadId >>= haltTaskIn reason

haltIfNeededWith :: (VoidFallible -> VoidFallible)
haltIfNeededWith = haltIfNeededWith' True

haltIfNeeded :: VoidFallible
haltIfNeeded = haltIfNeededWith' False success

tieHATLineTo :: ThreadId -> HATLine -> VoidFallible
tieHATLineTo threadID hatLine = modifyHATIfPresent threadID $
	\hat@HAT{..} -> return hat{ hatLines = hatLine:hatLines }

tieHATLine :: HATLine -> VoidFallible
tieHATLine line = tryIO myThreadId >>= flip tieHATLineTo line

cutHATLineIfOpen :: String -> HATLine -> VoidFallible
cutHATLineIfOpen reason HATLine{..} = do
	isOpen <- hatLineIsOpen
	if isOpen
		then cutHATLine reason
		else success
