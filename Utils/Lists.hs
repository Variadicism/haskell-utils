module Utils.Lists (
	mapIndexed,
	findLastIndex,
	orEmpty,
	(=/\/=),
	showWithConjunction,
	showWithOr,
	showWithAnd,
	listIf,
	wrap,
	ifNotNull,
	pairMap,
	range,
	shuffle,
	shuffleWith,
	takeWhileIncl,
	takeUntilIncl,
	insertAt,
	removeAt,
	updateAt
) where

import Data.Char (toLower)
import Data.List (findIndex, intercalate)
import System.Random (RandomGen, getStdGen)
import System.Random.Shuffle (shuffle')

mapIndexed :: (Int -> a -> b) -> [a] -> [b]
mapIndexed f l = map (uncurry f) $ foldr foldIndexed [] l where
	foldIndexed next prev = (length l - 1 - length prev, next) : prev

findLastIndex :: Eq a => (a -> Bool) -> [a] -> Maybe Int
findLastIndex matcher list = do
	let indexFromEnd = findIndex matcher $ reverse list
	fmap (length list - 1-) indexFromEnd

orEmpty :: Bool -> [a] -> [a]
orEmpty _ [] = []
orEmpty condition value = if condition then value else []

(=/\/=) :: String -> (String -> Bool)
(=/\/=) left = (map toLower left==) . map toLower

showWithConjunction :: Show a => String -> [a] -> String
showWithConjunction _ [] = ""
showWithConjunction _ [element] = show element
showWithConjunction conj [first, second] = show first ++ " " ++ conj ++ " " ++ show second
showWithConjunction conj threeOrMore = (intercalate ", " $ map show $ init threeOrMore) ++ ", " ++ conj ++ " " ++ (show $ last threeOrMore)

showWithOr :: Show a => ([a] -> String)
showWithOr = showWithConjunction "or"

showWithAnd :: Show a => ([a] -> String)
showWithAnd = showWithConjunction "and"

listIf :: [(Bool, a)] -> [a]
listIf conditionalItems = [ item | (conditional, item) <- conditionalItems, conditional ]

wrap :: [a] -> [a] -> [a]
wrap wrapper = (wrapper++) . (++wrapper)

ifNotNull :: ([a] -> [a]) -> [a] -> [a]
ifNotNull f list = if null list then [] else f list

pairMap :: (a -> b) -> ([a] -> [(b, a)])
pairMap f = map (\value -> (f value, value))

range :: Int -> Int -> [Int]
range end1 end2 =
	if end1 <= end2
		then [end1..end2]
		else [end1,(end1-1)..end2]

shuffleWith :: RandomGen r => r -> [a] -> [a]
shuffleWith _ [] = []
shuffleWith rng list = shuffle' list (length list) rng

shuffle :: [a] -> IO [a]
shuffle [] = return []
shuffle list = do
	rng <- getStdGen
	return $ shuffleWith rng list

takeWhileIncl :: (a -> Bool) -> [a] -> [a]
takeWhileIncl meets list = do
	let (before, atAndAfter) = span meets list
	before ++ take 1 atAndAfter

takeUntilIncl :: (a -> Bool) -> ([a] -> [a])
takeUntilIncl meets = takeWhileIncl (not . meets)

insertAt :: Int -> a -> [a] -> [a]
insertAt 0 e list = e : list
insertAt _ _ [] = []
insertAt i e (first:rest) = first : insertAt (i - 1) e rest

removeAt :: Int -> [a] -> [a]
removeAt _ [] = []
removeAt 0 (_:rest) = rest
removeAt i (first:rest) = first : removeAt (i - 1) rest

updateAt :: Int -> (a -> a) -> ([a] -> [a])
updateAt index update =
	map (\(i, e) -> if i == index then update e else e) . zip [0..]