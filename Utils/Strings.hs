module Utils.Strings (
	r,
	trimmed,
	unindented,
	parenthesize,
	escape,
	pluralCount,
	aOrAn
) where

import Data.Char (isLetter)
import Data.List.Extra (replace)
import qualified Data.Text as Text
import qualified GHC.Err as GHC (error)
import Language.Haskell.TH.Quote (QuasiQuoter(..))
import qualified NeatInterpolation as Neat (trimming, untrimming)
import qualified Text.RawString.QQ as RawString

-- private utils

unpackTextForExpFromQuasiQuoter :: String -> QuasiQuoter -> QuasiQuoter
unpackTextForExpFromQuasiQuoter name qq = QuasiQuoter {
	quoteExp = \string -> [|Text.unpack $(quoteExp qq string)|],
	quotePat = unsupported "patterns",
	quoteType = unsupported "types",
	quoteDec = unsupported "declarations" }
	where
		unsupported thing = GHC.error $
			"Quasi-quoting " ++ thing ++ " is not supported by this " ++ name ++ " QuasiQuoter!"

-- exported utils

r :: QuasiQuoter
r = RawString.r

trimmed :: QuasiQuoter
trimmed = unpackTextForExpFromQuasiQuoter "trimming" Neat.trimming

unindented :: QuasiQuoter
unindented = unpackTextForExpFromQuasiQuoter "untrimming" Neat.untrimming

parenthesize :: String -> String
parenthesize = ('(':) . (++")")

escape :: [Char] -> String -> String
escape chars string = foldl (\s c -> replace [c] ['\\', c] s) string chars

pluralCount :: Int -> String -> String
pluralCount count singular =
	if count == 1 then "1 " ++ singular else show count ++ " " ++ singular ++ "s"

aOrAn :: String -> String
aOrAn [] = ""
aOrAn s@(next:_) =
	-- Writing the vowels as "aeiou" is SOMEHOW ambiguous with OverloadedStrings. >:(
	if next `elem` ['a', 'e', 'i', 'o', 'u'] then "an " ++ s
	else if isLetter next then "a " ++ s
	else s