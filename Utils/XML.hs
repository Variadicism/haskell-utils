module Utils.XML (
	parseXML
) where

import Utils.Fallible
import Utils.Messaging

import Data.ByteString (ByteString)
import Data.ByteString.Char8 (unpack)
import Xeno.Types (XenoException(..))
import qualified Xeno.DOM as Xeno (Node, parse)

type Node = Xeno.Node

parseXML :: ByteString -> Fallible Node
parseXML raw = do
	let parseResults = Xeno.parse raw
	let xenoExToMessage ex =
		case ex of
			XenoStringIndexProblem index byteString ->
				"There was an invalid string index processed within Xeno: " ++
				msgShow byteString ++ " has no index " ++ show index ++ "!"
			XenoParseError index message ->
				"Xeno could not parse this XML input string at index " ++ show index ++ ": " ++ unpack message
			XenoExpectRootNode -> "Xeno was unable to find the root node of this XML document!"
	mapError xenoExToMessage $ fromEither parseResults
