module Utils.Maps.Ordered (
	keys,
	values,
	map,
	concat,
	headMaybe,
	head,
	lastMaybe,
	last
) where

import Prelude hiding (concat, head, last, map)
import qualified Prelude

import Utils.Tuples (mapSnd2)

import Data.Map.Ordered ((<>|), OMap)
import qualified Data.Map.Ordered as OMap
import Data.Maybe (fromJust)

keys :: OMap k v -> [k]
keys = Prelude.map fst . OMap.assocs

values :: OMap k v -> [v]
values = Prelude.map snd . OMap.assocs

map :: Ord k => (a -> b) -> (OMap k a -> OMap k b)
map mapper = OMap.fromList . Prelude.map (mapSnd2 mapper) . OMap.assocs

concat :: (Foldable l, Ord k) => l (OMap k v) -> OMap k v
concat = foldl (<>|) OMap.empty

headMaybe :: (OMap k v -> Maybe (k, v))
headMaybe = flip OMap.elemAt 0

head :: (OMap k v -> (k, v))
head = fromJust . headMaybe

lastMaybe :: OMap k v -> Maybe (k, v)
lastMaybe map = map `OMap.elemAt` (OMap.size map - 1)

last :: (OMap k v -> (k, v))
last = fromJust . lastMaybe