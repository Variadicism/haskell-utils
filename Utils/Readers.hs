module Utils.Readers (
	ReadResult,
	OneReadResult,
	Reader,
	OneReader,
	SureReader,
	AllReader,
	SureAllReader,
	Skipper,
	MultiReader,
	(<?<),
--	(.<<),
--	(o<<),
	readAll,
	readString,
	readInt,
	readDouble
) where

import Utils.Fallible
import Utils.Messaging

import Prelude hiding (drop)

import Data.Text.Lazy (Text, drop, pack, isPrefixOf)

type ReadResult m a = m (a, Text)

type OneReadResult a = ReadResult Fallible a

type Reader m a = Text -> ReadResult m a

type OneReader a = Reader Fallible a

type SureReader a = Text -> (a, Text)

type AllReader a = Text -> Fallible a

type SureAllReader a = Text -> a

type Skipper = Text -> Fallible Text

type MultiReader a = Reader [] a

(<?<) :: Monad m => Reader m b -> ReadResult m a -> ReadResult m (a, b)
reader <?< readResults = do
	(previouslyRead, afterPrevious) <- readResults
	(newlyRead, afterRead) <- reader afterPrevious
	return $ ((previouslyRead, newlyRead), afterRead)

readAll :: OneReader a -> AllReader a
readAll reader = \toParse -> do
	(valueRead, remainingText) <- reader toParse
	case remainingText of
		"" -> return valueRead
		_ -> Error $ "After a successful parse of the beginning, the ending " ++
			msg remainingText ++ " could not be parsed."

readString :: String -> Skipper
readString prefix = \toParse ->
	ifElseError ((pack prefix) `isPrefixOf` toParse)
		(drop (fromIntegral $ length prefix) toParse)
		(msg toParse ++ " does not begin with " ++ msg prefix ++ "!")

readInt :: String -> Fallible Int
readInt stringValue =
	case reads stringValue :: [(Int, String)] of
		[(readInt, "")] -> return readInt
		_ -> Error ("I expected an integer, but got \"" ++ stringValue ++ "\"!")

readDouble :: String -> Fallible Double
readDouble stringValue =
	case reads stringValue :: [(Double, String)] of
		[(readDouble, "")] -> return readDouble
		_ -> Error ("I expected a rational number, but got \"" ++ stringValue ++ "\"!")

-- (.<<) ::

-- (o<<) ::

-- /*/ :: Monad m => (text -> m (a, Text)) -> Text

-- /+/ :: Monad m => (Text -> m (a, Text)) ->