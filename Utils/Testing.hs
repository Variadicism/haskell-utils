{-# LANGUAGE NoImplicitPrelude #-}

module Utils.Testing (
	TestCategory(..),
	test,
	assert0,
	assert1,
	assert2,
	assert3,
	assert4,
	assert5,
	hasErrorElse,
	hasValueElse,
	isEqualElse,
	isNullElse,
	isMapNullElse
) where

import Utils.Collective
import Utils.Fallible
import Utils.Fallible.Trans (Void, VoidFallible, falM, falT)
import Utils.Lists (ifNotNull, wrap)
import Utils.Messaging (TerminalColor(..), debugM, msg, msgShowList, tColor)
import Utils.Tuples (mapSnd2)

import Control.Monad (when)
import Data.Map (Map)
import qualified Data.Map as Map (null)
import System.Exit (ExitCode(ExitFailure), exitWith)

-- types

data TestCategory =
	TestCategories String [TestCategory] |
	PureTests String [(String, Fallible ())] |
	IOTests String [(String, VoidFallible)]

-- private

getTestCategoryTitle :: TestCategory -> String
getTestCategoryTitle (TestCategories title _) = title
getTestCategoryTitle (PureTests title _) = title
getTestCategoryTitle (IOTests title _) = title

runTest :: String -> VoidFallible -> IO Bool
runTest name toRun = do
	debugM $ "Output of " ++ msg name ++ ":\n" ++ replicate 20 '=' ++ "\n"
	result <- falM toRun
	case result of
		Error error -> do
			debugM $ msg name ++ " FAILED:\n" ++ wrap "\"" error
			putStrLn $ tColor [Red, Bold] $ "The " ++ name ++ " test failed:\n" ++ error
			return False
		Falue _ -> do
			debugM $ msg name ++ " PASSED:"
			putStrLn $ tColor [LightGreen] $ "The " ++ name ++ " test passed!"
			return True

summarizeTests :: Int -> Int -> String -> Void
summarizeTests failCount testCount categoryTitle =
	if failCount > 0
		then putStrLn $ tColor [Red, Bold] $
			show failCount ++ " of " ++ totalTestSummary ++ " failed! :("
		else putStrLn $ tColor [Green, Bold] $
			"All " ++ totalTestSummary ++ " passed! :)"
	where totalTestSummary = show testCount ++ ifNotNull (' ':) categoryTitle ++ " tests"

runTests :: String -> [(String, VoidFallible)] -> IO Bool
runTests title tests = do
	passes <- mapM (uncurry runTest) tests
	summarizeTests (length $ filter not passes) (length tests) title
	return $ and passes

showLined :: (Show a, Functor t, Foldable t) => t a -> String
showLined = intercalate "\n" . map show

failAssertCount :: (Show a, Functor t, Foldable t, Show (t a)) => Int -> String -> t a -> Fallible b
failAssertCount expected valuesDescription values = Error $
	(if expected == 0 then "No" else show expected) ++ " " ++ valuesDescription ++
	" were expected, but I found " ++
	(if null values then "none!" else show (length values) ++ ":\n" ++ showLined values)

-- exported

-- In the future, generate an MVar for each test, run all tests concurrently, and take from them in order.
-- This will allow each test to run asynchronously, yet output in order!
test :: TestCategory -> Void
test category = do
	let test' :: TestCategory -> IO Bool = \category -> do
		putStrLn $ tColor [Bold] $ "Running " ++ getTestCategoryTitle category ++ " tests:"
		case category of
			TestCategories _ subcategories -> fmap and $ mapM test' subcategories
			PureTests title tests -> runTests title $ map (mapSnd2 falT) tests
			IOTests title tests -> runTests title tests
	pass <- test' category
	when (not pass) $ exitWith $ ExitFailure 1

assert0 :: (Show a, Functor t, Foldable t, Show (t a)) => String -> t a -> Fallible ()
assert0 valuesDescription values = if length values == 0
	then return ()
	else failAssertCount 0 valuesDescription values

assert1 :: (Show a, Functor t, Foldable t, Show (t a)) => String -> t a -> Fallible a
assert1 valueDescription values = if length values == 1
	then return $ values !! 0
	else failAssertCount 1 valueDescription values

assert2 :: (Show a, Functor t, Foldable t, Show (t a)) => String -> t a -> Fallible (a, a)
assert2 valuesDescription values = if length values == 2
	then return (values !! 0, values !! 1)
	else failAssertCount 2 valuesDescription values

assert3 :: (Show a, Functor t, Foldable t, Show (t a)) => String -> t a -> Fallible (a, a, a)
assert3 valuesDescription values = if length values == 3
	then return (values !! 0, values !! 1, values !! 2)
	else failAssertCount 3 valuesDescription values

assert4 :: (Show a, Functor t, Foldable t, Show (t a)) => String -> t a -> Fallible (a, a, a, a)
assert4 valuesDescription values = if length values == 4
	then return (values !! 0, values !! 1, values !! 2, values !! 3)
	else failAssertCount 4 valuesDescription values

assert5 :: (Show a, Functor t, Foldable t, Show (t a)) => String -> t a -> Fallible (a, a, a, a, a)
assert5 valuesDescription values = if length values == 5
	then return (values !! 0, values !! 1, values !! 2, values !! 3, values !! 4)
	else failAssertCount 5 valuesDescription values

hasValueElse :: Show e => FallibleI e a -> String -> Fallible ()
hasValueElse fal assertionString =
	fallible
		(\error -> Error $ assertionString ++ "\nThis was the unexpected error: " ++ show error)
		(const $ return ())
		fal

hasErrorElse :: Show a => Fallible a -> String -> Fallible ()
hasErrorElse fal assertionString =
	fallible
		(const $ return ())
		(\value -> Error $ assertionString ++ "\nThis was the unexpected value: " ++ show value)
		fal

isEqualElse :: (Show a, Eq a) => (a, a) -> String -> Fallible ()
isEqualElse (expected, actual) meaning = 
	if expected /= actual then
		Error (meaning ++ "\nI expected     " ++ show expected ++ ".\nInstead, I got " ++ show actual ++ ".\n")
	else return ()

isNullElse :: Show a => [a] -> String -> Fallible ()
isNullElse list meaning =
	if null list
		then return ()
		else Error (meaning ++ "\nThis list was expected to be empty: " ++ msgShowList list)

isMapNullElse :: (Show k, Show v) => Map k v -> String -> Fallible ()
isMapNullElse map meaning =
	if Map.null map
		then return ()
		else Error (meaning ++ "\nThis map was expected to be empty: " ++ show map)
