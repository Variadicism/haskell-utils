module Utils.Files (
	areIdentical,
	readHaskellDataFile,
	readHaskellDataFileOr,
	writeHaskellDataFile,
	serveDirHTTP,
	serveDirHTTPIndexedBy
) where

import Utils.Fallible
import Utils.Fallible.Trans
import Utils.Processes (HATProcess, exec, silently, spawn)

import Data.Char (isSpace)
import System.Process (proc)

areIdentical :: FilePath -> FilePath -> IOFallible Bool
areIdentical path1 path2 = do
	(stdout, _) <- exec ("diff " ++ show path1 ++ " and " ++ show path2) $
		silently $ proc "diff" [path1, path2]
	return $ any (not . isSpace) stdout

readHaskellDataFile :: (Show a, Read a) => FilePath -> IO a
readHaskellDataFile path = readFile path >>= readIO

readHaskellDataFileOr :: (Show a, Read a) => FilePath -> IO a -> IO (a, Maybe String)
readHaskellDataFileOr path getDefault = do
	contentsOrError <- falM $ tryIO $ readHaskellDataFile path
	case contentsOrError of
		Error error -> fmap (, Just error) getDefault
		Falue value -> return (value, Nothing)

writeHaskellDataFile :: Show a => FilePath -> (a -> Void)
writeHaskellDataFile path = writeFile path . show

serveDirHTTP :: FilePath -> Int -> IOFallible HATProcess
serveDirHTTP dir port = spawn ("serving " ++ show dir ++ " via HTTP") $
	silently $ proc "webfsd" ["-F", "-r", dir, "-p", show port]

serveDirHTTPIndexedBy :: FilePath -> FilePath -> Int -> IOFallible HATProcess
serveDirHTTPIndexedBy indexPath dir port =
	spawn ("serving " ++ show dir ++ " via HTTP indexed by " ++ show indexPath) $
		silently $ proc "webfsd" ["-F", "-r", dir, "-f", indexPath, "-p", show port]
