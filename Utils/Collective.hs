module Utils.Collective (
	module Basics,
	(!!?),
	(!!),
	map,
	null,
	head,
	last,
	concat,
	intercalate
) where

import Prelude as Basics hiding ((!!), concat, head, last, map, null)

import Data.Maybe (fromJust)

(!!?) :: Foldable t => t a -> (Int -> Maybe a)
(!!?) l targetIndex = snd $ foldl seek (0, Nothing) l where
	seek (i, prev) value = (i + 1,) $
		if i == targetIndex then Just value else prev

(!!) :: Foldable t => t a -> (Int -> a)
(!!) l i = fromJust $ l !!? i

map :: Functor t => (a -> b) -> t a -> t b
map = fmap

-- mapIndexed requires an analog to `cons`. No Haskell Prelude class seems to define that. :(

null :: Foldable t => t a -> Bool
null = (0==) . length

head :: Foldable t => (t a -> Maybe a)
head = (!!? 0)

last :: Foldable t => (t a -> Maybe a)
last l = if null l then Nothing else l !!? (length l - 1)

concat :: (Foldable o, Monoid (i a)) => (o (i a) -> i a)
concat = foldr prepend mempty where
	prepend next prev = next <> prev

intercalate :: (Foldable o, Monoid (i a)) => i a -> (o (i a) -> i a)
intercalate insert = snd . foldr concatAppendingIf1 (True, mempty) where
	concatAppendingIf1 next (isFirst, result) = (False,) $
		if isFirst then next <> result else next <> insert <> result

-- The biggest problem here is that we can't create a new instance of the Foldables.
-- So, any function that returns one of these, including `map` and `tail`, won't work.