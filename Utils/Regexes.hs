module Utils.Regexes (
	urlRegex
) where

import Utils.Regex (Regex, re)

urlRegex :: Regex
urlRegex = [re|([a-z]+://)[-a-zA-Z0-9@:%._\+#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_+.~#?&//=]*)|data:[a-z\/\-]+(;base64)?,\S+|]
