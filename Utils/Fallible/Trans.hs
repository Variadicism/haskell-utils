module Utils.Fallible.Trans (
	Void,
	FallibleTI(..),
	FallibleT,
	IOFallibleI,
	IOFallible,
	VoidFallibleI,
	VoidFallible,
	errorT,
	tryIO,
	success,
	mapErrorT,
	prependToErrorT,
	falT,
	falM,
	mFal,
	holdFal,
	fallibleT,
	doFallibleMain,
	elseErr
) where

import Utils.Fallible
import Utils.Messaging

import Control.Monad.IO.Class (MonadIO, liftIO)
import Data.Functor ((<&>))
import GHC.Records (HasField, getField)
import System.Exit (ExitCode(..), exitWith)
import System.IO.Error (tryIOError)

-- types

type Void = IO ()

{- I really REALLY wanted to implement this as ErrorT e | FalueT (m v) to emulate Fallible and have
   nice constructor to use in code, but I can't figure out how to implement (>>=) because the FalT
   made by the right-hand function ends up inside the m and I can't get it out. :( -}
data FallibleTI e m v = FallibleTI (m (FallibleI e v))

type FallibleT m v = FallibleTI String m v

type IOFallibleI e v = FallibleTI e IO v

type IOFallible v = IOFallibleI String v

type VoidFallibleI e = IOFallibleI e ()

type VoidFallible = IOFallible ()

-- instances

instance (Eq e, Eq (m (FallibleI e v))) => Eq (FallibleTI e m v) where
	(==) (FallibleTI mf1) (FallibleTI mf2) = mf1 == mf2

instance (Show e, Show (m (FallibleI e v))) => Show (FallibleTI e m v) where
	show (FallibleTI mf) = show mf

instance Functor m => Functor (FallibleTI e m) where
	fmap f (FallibleTI mfv) = FallibleTI $ fmap (fmap f) mfv

instance (IsError e, Applicative m) => Applicative (FallibleTI e m) where
	pure = FallibleTI . pure . Falue
	(<*>) :: FallibleTI e m (a -> b) -> FallibleTI e m a -> FallibleTI e m b
	(<*>) (FallibleTI mfvf) (FallibleTI mfPrev) = FallibleTI $
		fmap (<*>) mfvf <*> mfPrev

-- instance Bifunctor isn't viable because it requires that the target be * -> * -> *.

instance (IsError e, Monad m) => Monad (FallibleTI e m) where
	(FallibleTI mfv) >>= f = FallibleTI do
		fv <- mfv
		case fv of
			Error e -> return $ Error e
			Falue v -> do
				let FallibleTI mfvNext = f v
				mfvNext

instance MonadIO (FallibleTI String IO) where
	liftIO = FallibleTI . fmap Falue

-- Damn it.
-- instance Applicative m => HasField "t" (e -> FallibleI e v) (e -> FallibleTI e m v)

instance Monad m => HasField "orDoM" (FallibleTI e m v) ((e -> m v) -> m v) where
	getField (FallibleTI mfv) f = do
		fv <- mfv
		case fv of
			Error e -> f e
			Falue v -> return v

-- exported

errorT :: Applicative m => e -> FallibleTI e m v
errorT = FallibleTI . pure . Error

tryIO :: IO a -> IOFallible a
tryIO operation = FallibleTI do
	ioErrorOrValue <- tryIOError operation
	return $ either (Error . show) Falue ioErrorOrValue

falT :: Applicative m => (FallibleI e v -> FallibleTI e m v)
falT = FallibleTI . pure

falM :: Monad m => FallibleTI e m v -> m (FallibleI e v)
falM (FallibleTI mfv) = mfv

mFal :: Functor m => m v -> FallibleTI e m v
mFal = FallibleTI . fmap Falue

holdFal :: Monad m => (FallibleTI e m v -> FallibleTI e m (FallibleI e v))
holdFal = mFal . falM

fallibleT :: Monad m => (e -> m r) -> (v -> m r) -> FallibleTI e m v -> m r
fallibleT emf vmf (FallibleTI mfv) = mfv >>= fallible emf vmf 

success :: IsError e => VoidFallibleI e
success = return ()

mapErrorT :: Functor m => (e -> e2) -> FallibleTI e m v -> FallibleTI e2 m v
mapErrorT ef (FallibleTI mfal) = FallibleTI $ mfal <&> \case
	Error e -> Error $ ef e
	Falue v -> Falue v

prependToErrorT :: (IsError e, Functor m) => String -> FallibleTI e m v -> FallibleTI e m v
prependToErrorT prefix = mapErrorT (prepend prefix)

doFallibleMain :: VoidFallible -> Void
doFallibleMain wrappedMain = do
	debugM "Starting return main..."
	wrappedMainResult <- falM wrappedMain
	let failMain = \error -> do
		err error
		debugM "This program failed and will exit with status code 1. :("
		exitWith $ ExitFailure 1
	let printDoneDebugMessage = debugM "All clear! This program completed successfully!"
	fallible failMain (const printDoneDebugMessage) wrappedMainResult

elseErr :: VoidFallible -> Void
elseErr iofal = do
	fal <- falM iofal
	fallible err return fal