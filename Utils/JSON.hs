module Utils.JSON (
	ToJSONObject,
	FromJSON,
	FromJSONObject,
	toJSONObject,
	fromJSON,
	fromJSONObject,
	jsonObject,
	objectsOnly,
	arraysOnly,
	stringsOnly,
	numbersOnly,
	valueOnly,
	valueMatchesOnly,
	valueIsOnly,
	extractValueOr,
	extractValue,
	encode
) where

import Utils.Bytes
import Utils.Fallible
import Utils.Messaging

import Data.Aeson (ToJSON, toJSON)
import qualified Data.Aeson as Aeson
import qualified Data.Aeson.Key as Aeson (fromString)
import qualified Data.Aeson.KeyMap as Aeson (lookup)
import qualified Data.Aeson.Types as Aeson
import qualified Data.ByteString.Lazy.UTF8 as UTF8BS
import qualified Data.Scientific as Scientific
import Data.Text.Lazy (Text)
import qualified Data.Text.Lazy as Text
import qualified Data.Vector as Vector

-- types

class ToJSONObject o where
	toJSONObject :: o -> Aeson.Object

class FromJSON v where
	fromJSON :: Aeson.Value -> Fallible v

class FromJSONObject o where
	fromJSONObject :: Aeson.Object -> Fallible o

-- instances

instance ToBytes Aeson.Value where
	toBytes = Aeson.encode

instance FromBytes Aeson.Value where
	fromBytes = fromEither . Aeson.eitherDecode

instance ToBytes Aeson.Object where
	toBytes = Aeson.encode . Aeson.Object

-- exported utils

jsonObject :: [Aeson.Pair] -> Aeson.Object
jsonObject pairs = case Aeson.object pairs of
	Aeson.Object object -> object
	_ -> -- This code is dead because Aeson.object always returns an object; the type signature is just dumb.
		error "Aeson.object made something besides an object!"

objectsOnly :: Aeson.Value -> Fallible Aeson.Object
objectsOnly (Aeson.Object object) = return object
objectsOnly _ = Error "The given JSON value was not an object!"

arraysOnly :: Aeson.Value -> Fallible [Aeson.Value]
arraysOnly (Aeson.Array array) = return $ Vector.toList array
arraysOnly _ = Error "The given JSON value was not an array!"

stringsOnly :: Aeson.Value -> Fallible Text
stringsOnly (Aeson.String text) = return $ Text.fromStrict text
stringsOnly _ = Error "The given JSON value was not a string!"

numbersOnly :: Aeson.Value -> Fallible Float
numbersOnly (Aeson.Number number) = return $ Scientific.toRealFloat number
numbersOnly _ = Error "The given JSON value was not a number!"

valueOnly :: String -> (Aeson.Value -> Fallible ()) -> Aeson.Object -> Fallible Aeson.Object
valueOnly key valueChecker object = 
	case Aeson.lookup (Aeson.fromString key) object of
		Nothing -> Error $ "This JSON object doesn't contain the key " ++ msg key ++ "!"
		Just value -> fmap (const object) $ valueChecker value

valueMatchesOnly :: String -> (Aeson.Value -> Bool) -> (Aeson.Object -> Fallible Aeson.Object)
valueMatchesOnly key valueMatcher = valueOnly key (\value ->
	if valueMatcher value
		then return ()
		else Error $ "The value " ++ msgShow value ++ " did not match expectations!" )

valueIsOnly :: ToJSON j => String -> j -> (Aeson.Object -> Fallible Aeson.Object)
valueIsOnly key value = valueMatchesOnly key (toJSON value==)

extractValueOr :: String -> String -> Aeson.Object -> Fallible Aeson.Value
extractValueOr key error = fromMaybe error . Aeson.lookup (Aeson.fromString key)

extractValue :: String -> (Aeson.Object -> Fallible Aeson.Value)
extractValue key = extractValueOr key $ "The " ++ msg key ++ " was missing!"

encode :: ToJSON j => j -> String
encode = UTF8BS.toString . Aeson.encode
