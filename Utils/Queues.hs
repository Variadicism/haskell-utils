module Utils.Queues (
	Queue,
	IOQueue,
	TimedQueue,
	newQueue,
	enqueue,
	dequeue,
	newIOQueue,
	enqueueIO,
	dequeueIO,
	newTimedQueue,
	enqueueTimed
) where

import Utils.Fallible.Trans
import Utils.HATs (haltableAsync)
import Utils.Messaging (debugM)

import Control.Concurrent (threadDelay)
import Data.IORef (IORef, atomicModifyIORef, newIORef, readIORef)

type Queue a = [a]
type IOQueue a = IORef (Queue a)

-- private utils

dequeueTimed :: TimedQueue a -> VoidFallible
dequeueTimed timedQueue@TimedQueue{..} = do
	items <- tryIO $ readIORef tqItemsRef
	case fst $ dequeue items of
		Nothing -> debugM $ "Timed queue " ++ tqName ++ " has nothing more to dequeue."
		Just item -> do
			haltableAsync ("timed dequeue from " ++ tqName) $ do
				tqToDo item
				tryIO $ threadDelay tqGapMicroseconds
				tryIO $ dequeueIO tqItemsRef
				dequeueTimed timedQueue
			success

-- exported utils

newQueue :: Queue a
newQueue = []

enqueue :: Queue a -> a -> Queue a
enqueue queue new = queue ++ [new]

dequeue :: Queue a -> (Maybe a, Queue a)
dequeue (first:rest) = (Just first, rest)
dequeue [] = (Nothing, [])

newIOQueue :: IO (IOQueue a)
newIOQueue = newIORef []

enqueueIO :: IOQueue a -> a -> IO (Queue a)
enqueueIO queue new = atomicModifyIORef queue $ \q -> (enqueue q new, enqueue q new)

dequeueIO :: IOQueue a -> IO (Maybe a, Queue a)
dequeueIO queue = atomicModifyIORef queue $ \q -> (snd $ dequeue q, dequeue q)

data TimedQueue a = TimedQueue {
	tqName :: String,
	tqGapMicroseconds :: Int,
	tqToDo :: a -> VoidFallible,
	tqItemsRef :: IOQueue a }

newTimedQueue :: String -> Float -> (a -> VoidFallible) -> IO (TimedQueue a)
newTimedQueue name gapSeconds toDo = do
	itemsRef <- newIORef []
	return TimedQueue {
		tqName = name,
		tqGapMicroseconds = floor $ gapSeconds * 1000000,
		tqToDo = toDo,
		tqItemsRef = itemsRef }

enqueueTimed :: TimedQueue a -> a -> VoidFallible
enqueueTimed timedQueue@TimedQueue{..} item = do
	queueWithNewItem <- tryIO $ enqueueIO tqItemsRef item
	if length queueWithNewItem == 1 then do
		debugM $ "First item enqueued in timed queue " ++ tqName ++ "; starting dequeueing..."
		dequeueTimed timedQueue
	else success
