module Utils.Populace.Populace2 (
	Populace2(..),
	toList,
	toP1,
	dualton,
	map,
	mapIndexed,
	filter,
	tail,
	init,
	last,
	cons,
	cons1,
	uncons,
	snoc,
	sort,
	assert,
	assume,
	concat
) where

import Prelude hiding (concat, head, init, last, map, tail)
import qualified Prelude

import Utils.Fallible hiding (assert, assume)
import qualified Utils.Fallible as Fallible
import Utils.Populace (Populace)
import qualified Utils.Populace as P

import qualified Data.List as List
import GHC.Records (HasField, getField)

-- types

data Populace2 a = Populace2 { head :: a, next :: a, tailTail :: [a] }
	deriving (Eq, Ord)

-- instances

instance Show a => Show (Populace2 a) where
	show = show . toList

instance Semigroup (Populace2 a) where
	(<>) p1 p2 = p1{ tailTail = p1.tailTail ++ p2.l }

instance Foldable Populace2 where
	foldr f s p = f p.head $ f p.next $ Prelude.foldr f s p.tailTail
	length p = length p.tailTail + 2

instance Functor Populace2 where
	fmap f p = Populace2 (f p.head) (f p.next) (Prelude.map f p.tailTail)

-- Applicative and Monad have no reasonable implementation of `pure` or `return`.

-- Like Populace, Monoid has no reasonable implementation of `mempty`.

instance Traversable Populace2 where
	traverse :: Applicative f => (a -> f b) -> Populace2 a -> f (Populace2 b)
	traverse f p = fmap Populace2 (f p.head) <*> f p.next <*> traverse f p.tailTail

instance HasField "tail" (Populace2 a) (Populace a) where
	getField = tail

instance HasField "init" (Populace2 a) (Populace a) where
	getField = init

instance HasField "last" (Populace2 a) a where
	getField = last

instance Ord a => HasField "sorted" (Populace2 a) (Populace2 a) where
	getField = sort

instance HasField "l" (Populace2 a) [a] where
	getField = toList

instance HasField "p1" (Populace2 a) (Populace a) where
	getField = toP1

-- exported

toList :: Populace2 a -> [a]
toList p = p.head:p.next:p.tailTail

toP1 :: Populace2 a -> Populace a
toP1 p = P.Populace p.head $ p.next:p.tailTail

-- Yes, I'm making up words again. Try to stop me! :P
dualton :: a -> a -> Populace2 a
dualton head next = Populace2 head next []

map :: ((a -> b) -> Populace2 a -> Populace2 b)
map = fmap

mapIndexed :: (Int -> a -> b) -> Populace2 a -> Populace2 b
mapIndexed f p = map (uncurry f) $
		Populace2 (0, p.head) (1, p.next) $
		foldr foldIndexed [] p.tailTail where
	foldIndexed next prev = (length p - 1 - length prev, next) : prev

tail :: Populace2 a -> Populace a
tail p = P.Populace p.next p.tailTail

init :: Populace2 a -> Populace a
init p = P.Populace p.head $ case p.tailTail of
	[] -> []
	[_] -> [p.next]
	(_:_) -> p.next : Prelude.init p.tailTail

last :: Populace2 a -> a
last p = case p.tailTail of
	[] -> p.next
	_ -> Prelude.last p.tailTail  -- Prelude.last is safe because `tail` is guaranteed nonempty.

cons :: a -> Populace2 a -> Populace2 a
cons new p = Populace2 new p.head p.tail.l

cons1 :: a -> Populace a -> Populace2 a
cons1 new p = Populace2 new p.head p.tail

uncons :: Populace2 a -> (a, Populace a)
uncons p = (p.head, P.Populace p.next p.tailTail)

snoc :: Populace2 a -> a -> Populace2 a
snoc p2 last = p2{ tailTail = p2.tailTail ++ [last] }

sort :: Ord a => Populace2 a -> Populace2 a
-- `assume` is safe because sorting does not remove elements.
sort = assume . List.sort . toList

assert :: [a] -> e -> FallibleI e (Populace2 a)
assert list error = case list of
	(head:(next:tailTail)) -> Falue $ Populace2 head next tailTail
	_ -> Error error

assume :: [a] -> Populace2 a
assume l = Fallible.assume $ assert l "This list has a populace of <2!"

concat :: Populace2 (Populace2 a) -> Populace2 a
concat p = Populace2 p.head.head p.head.next $ p.head.tailTail ++ concatMap toList p.tailTail