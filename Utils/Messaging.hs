{-# LANGUAGE CPP #-}
{-# OPTIONS_GHC -fno-warn-unused-imports #-}
-- Debug.Trace is unused when VAR_DEBUG is undefined.
module Utils.Messaging (
	msg,
	msgShow,
	msgList,
	msgShowList,
	message,
	err,
	warn,
	debug,
	debugEq,
	debugEqS,
	debugM,
	debugIf,
	debugMIf,
	debugR,
	tColor,
	toTColor,
	tColorCode,
	TerminalColor(..),
	Messagable
) where

import Prelude hiding (either)

import Utils.Fallible
import Utils.Lists

import Data.Foldable (toList)
import Data.List (intercalate)
import Data.Sequence (dropWhileR, fromList)
import Data.Text.Lazy (Text, unpack)
import Debug.Trace (trace)

-- types

data TerminalColor =
	-- 0-2: formatting:
	Reset | Bold | Dim |
	-- skip 3
	Underlined | Blink |
	-- skip 6
	Invert | Hide |
	-- skip 9-20; reset formatting:
	NoBold | NoDim |
	-- skip 23
	NoUnderlined | NoBlink |
	-- skip 26
	NoInvert | NoHide |
	-- skip 29; foreground:
	Black | Red | Green | Yellow | Blue | Magenta | Cyan | LightGray |
	-- skip 38; reset foreground and background:
	ResetFG | BlackBG | RedBG | GreenBG | YellowBG | BlueBG | MagentaBG | CyanBG | LightGrayBG |
	-- skip 48; reset background:
	ResetBG |
	-- skip through 89; lighter foreground colors
	DarkGray | LightRed | LightGreen | LightYellow | LightBlue | LightMagenta | LightCyan | White |
	-- skip 98 and 99; lighter background colors
	DarkGrayBG | LightRedBG | LightGreenBG | LightYellowBG | LightBlueBG | LightMagentaBG | LightCyanBG | WhiteBG
		deriving (Eq, Ord, Enum, Show)

-- private

debugEq' :: (Eq a, Show a) => String -> a -> a -> Maybe String -> Bool
debugEq' actualName actual expected maybeExpectedName = do
	let result = actual == expected
	flip debug result $
		show actualName ++ " " ++ show actual ++ " == " ++
		show expected ++ maybe "" ((" " ++) . show) maybeExpectedName ++ " => " ++
		show result

-- exported

tColorCode :: TerminalColor -> Int
tColorCode color = do
	let gapSizes = [
		(Underlined, 1),
		(Invert, 1),
		(NoBold, 12),
		(NoUnderlined, 1),
		(NoInvert, 1),
		(Black, 1),
		(ResetFG, 1),
		(ResetBG, 1),
		(DarkGray, 40),
		(DarkGrayBG, 2)] :: [(TerminalColor, Int)]
	fromEnum color + (foldl (+) 0 $ map snd $ takeWhile ((<= color) . fst) gapSizes)

toTColor :: [TerminalColor] -> String
toTColor colors = "\x1B[" ++ (intercalate ";" $ map (show . tColorCode) colors) ++ "m"

tColor :: [TerminalColor] -> String -> String
tColor colors message = toTColor colors ++ message ++ toTColor [Reset]

err :: String -> IO ()  -- Monads depends on Messaging, so no Void!
err message = putStrLn $ tColor [Red, LightYellowBG, Bold] "ERROR:" ++ " " ++ tColor [Red, Bold] message

warn :: String -> IO ()  -- Monads depends on Messaging, so no Void!
warn message = putStrLn $ tColor [LightYellow, DarkGrayBG, Bold] "WARNING:" ++ " " ++ tColor [Yellow, Bold] message

message :: (String -> IO ())  -- Monads depends on Messaging, so no Void!
message = putStrLn . tColor [Bold]

debug :: String -> a -> a
#ifdef VAR_DEBUG
debug = trace
#else
debug _ value = value
#endif

debugM :: Monad m => String -> m ()
debugM message = debug message $ return ()

debugEqS :: (Eq a, Show a) => String -> a -> a -> Bool
debugEqS actualName actual expected = debugEq' actualName actual expected Nothing

debugEq :: (Eq a, Show a) => String -> a -> a -> (String -> Bool)
debugEq actualName actual expected = debugEq' actualName actual expected . Just

debugIf :: Bool -> String -> a -> a
debugIf condition message value =
	if condition
		then debug message value
		else value

debugMIf :: Monad m => Bool -> String -> m ()
debugMIf condition message =
	if condition
		then debugM message
		else return ()

debugR :: Show a => String -> a -> a
debugR message value = debug (message ++ show value) value

stringLimit :: Int
stringLimit = 200

msgShow :: Show s => (s -> String)
msgShow = msg . show

msgList :: Messagable a => ([a] -> String)
msgList [] = ""
msgList [element] = msg element
msgList [first, second] = msg first ++ " and " ++ msg second
msgList threeOrMore = (intercalate ", " $ map msg $ init threeOrMore) ++ ", and " ++ (msg $ last threeOrMore)

msgShowList :: Show a => ([a] -> String)
msgShowList = showWithAnd . map msgShow

class Messagable messagable where
	msg :: messagable -> String

instance Messagable Bool where
	msg :: Bool -> String
	msg bool = if bool then "True" else "False"

instance Show a => Messagable (Maybe a) where
	msg :: (Maybe a -> String)
	msg = maybe "Nothing" show

instance Messagable Text where
	msg :: (Text -> String)
	msg = msg . unpack

instance Messagable String where
	msg :: String -> String
	msg string = do
		let toMessage = if length string >= stringLimit
			then take (stringLimit - 5) string ++ "[...]"
			else string
		wrap "\"" toMessage

instance Show a => Messagable (Fallible a) where
	msg :: (Fallible a -> String)
	msg = fallible
		(("an error saying "++) . msg)
		(msg . dropWhile (=='"') . toList . dropWhileR (=='"') . fromList . show)

instance Messagable a => Messagable (a, a) where
	msg (a, b) = msg a ++ " and " ++ msg b

-- Messageable [a] cannot be instanced because of Messageable String. Use msgList.